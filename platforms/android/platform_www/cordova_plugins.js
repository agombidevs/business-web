cordova.define('cordova/plugin_list', function(require, exports, module) {
module.exports = [
    {
        "id": "onesignal-cordova-plugin.OneSignal",
        "file": "plugins/onesignal-cordova-plugin/www/OneSignal.js",
        "pluginId": "onesignal-cordova-plugin",
        "clobbers": [
            "OneSignal"
        ]
    },
    {
        "id": "cordova-plugin-network-information.network",
        "file": "plugins/cordova-plugin-network-information/www/network.js",
        "pluginId": "cordova-plugin-network-information",
        "clobbers": [
            "navigator.connection",
            "navigator.network.connection"
        ]
    },
    {
        "id": "cordova-plugin-network-information.Connection",
        "file": "plugins/cordova-plugin-network-information/www/Connection.js",
        "pluginId": "cordova-plugin-network-information",
        "clobbers": [
            "Connection"
        ]
    },
    {
        "id": "cordova-plugin-dialogs.notification",
        "file": "plugins/cordova-plugin-dialogs/www/notification.js",
        "pluginId": "cordova-plugin-dialogs",
        "merges": [
            "navigator.notification"
        ]
    },
    {
        "id": "cordova-plugin-dialogs.notification_android",
        "file": "plugins/cordova-plugin-dialogs/www/android/notification.js",
        "pluginId": "cordova-plugin-dialogs",
        "merges": [
            "navigator.notification"
        ]
    },
    {
        "id": "cordova-fabric-plugin.FabricPlugin",
        "file": "plugins/cordova-fabric-plugin/www/FabricPlugin.js",
        "pluginId": "cordova-fabric-plugin",
        "clobbers": [
            "window.fabric.core"
        ]
    },
    {
        "id": "cordova-fabric-plugin.FabricAnswersPlugin",
        "file": "plugins/cordova-fabric-plugin/www/FabricPlugin.Answers.js",
        "pluginId": "cordova-fabric-plugin",
        "clobbers": [
            "window.fabric.Answers"
        ]
    },
    {
        "id": "cordova-fabric-plugin.FabricCrashlyticsPlugin",
        "file": "plugins/cordova-fabric-plugin/www/FabricPlugin.Crashlytics.js",
        "pluginId": "cordova-fabric-plugin",
        "clobbers": [
            "window.fabric.Crashlytics"
        ]
    },
    {
        "id": "cordova-plugin-backbutton.Backbutton",
        "file": "plugins/cordova-plugin-backbutton/www/Backbutton.js",
        "pluginId": "cordova-plugin-backbutton",
        "clobbers": [
            "navigator.Backbutton"
        ]
    },
    {
        "id": "cordova-plugin-is-debug.IsDebug",
        "file": "plugins/cordova-plugin-is-debug/www/isDebug.js",
        "pluginId": "cordova-plugin-is-debug",
        "clobbers": [
            "cordova.plugins.IsDebug"
        ]
    }
];
module.exports.metadata = 
// TOP OF METADATA
{
    "onesignal-cordova-plugin": "2.0.10",
    "cordova-plugin-network-information": "1.3.1",
    "cordova-plugin-dialogs": "1.3.1",
    "cordova-plugin-whitelist": "1.3.2-dev",
    "cordova-fabric-plugin": "1.1.3",
    "cordova-plugin-backbutton": "0.3.0",
    "cordova-plugin-is-debug": "1.0.0"
};
// BOTTOM OF METADATA
});