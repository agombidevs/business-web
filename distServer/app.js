/* jshint node: true */
'use strict';
// var nodeStatic = require('node-static');

// var fileServer = new nodeStatic.Server('./dist', { indexFile: 'index.html'});

// require('http').createServer(function (request, response) {
//     request.addListener('end', function () {
//         fileServer.serve(request, response, function (err, result) {
//             if (err) { // There was an error serving the file
//                 console.error('Error serving ' + request.url + ' - ' + err.message);

//                 // Respond to the client
//                 response.writeHead(err.status, err.headers);
//                 response.end();
//             }
//         });
//     }).resume();
// }).listen(8080);


var express = require('express');
var app = express();

app.use(express.static(__dirname));

app.listen(process.env.PORT || 3000);

app.get('*', function(req, res) {
    res.sendFile(__dirname + '/index.html'); // load the single view file (angular will handle the page changes on the front-end)
});


console.log('App listening on port 3000');