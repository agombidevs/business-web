/**
 *  Welcome to your gulpfile!
 *  The gulp tasks are splitted in several files in the gulp directory
 *  because putting all here was really too long
 */

'use strict';

var gulp = require('gulp');
var wrench = require('wrench');
var del = require('del');
var awspublish = require('gulp-awspublish');
var parallelize = require('concurrent-transform');
var conf = require('./gulp/conf');
var cloudfront = require('gulp-cloudfront-invalidate-aws-publish');
var git = require('gulp-git');
var runSequence = require('run-sequence');

/**
 *  This will load all js or coffee files in the gulp directory
 *  in order to load all gulp tasks
 */
wrench.readdirSyncRecursive('./gulp').filter(function(file) {
    return (/\.(js|coffee)$/i).test(file);
}).map(function(file) {
    require('./gulp/' + file);
});



gulp.task('clean', function(cb) {
    del([conf.paths.dist + '/**/*',
        '!' + conf.paths.dist + '/.git'
    ]).then(function() {
        cb();
    });
});

/**
 *  Default task clean temporaries directories and launch the
 *  main optimization build task
 */
gulp.task('default', function(done) {
    runSequence('clean', 'build', function() {
        done();
    });
});


gulp.task('bump', function(done) {
    require('gulp-cordova-bump').run({ autofiles: true, patch: true }, function(newVersion){
        var gitMessage = 'Bump version to '+newVersion;
        gulp.src('./')
        .pipe(git.add())
        .pipe(git.commit(gitMessage))
        .on('end', function(){
            git.tag(newVersion,gitMessage, done);
        }); 

    });
});


//Deployment section



gulp.task('publish:production', function() {

    // Cloudfront 

    var cfSettings = {
        distribution: 'E3DIJPUAO4M5W', // Cloudfront distribution ID 
        wait: false, // Whether to wait until invalidation is completed (default: false) 
        indexRootPath: true // Invalidate index.html root paths (`foo/index.html` and `foo/`) (default: false) 
    };


    // create a new production publisher using S3 options 
    var publisher = awspublish.create({
        region: 'eu-central-1',
        params: {
            Bucket: 'web.agombi.co.il'
        },
        accessKeyId: 'AKIAI55ZPQKNRGOFPIDQ',
        secretAccessKey: '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'
    }, {
        cacheFileName: 'production-publish.cache'
    });

    return publish(publisher, cfSettings);
});


gulp.task('publish:staging', function() {


    // Cloudfront 

    var cfSettings = {
        distribution: 'E1GSZDAAY4E271', // Cloudfront distribution ID 
        wait: false, // Whether to wait until invalidation is completed (default: false) 
        indexRootPath: true // Invalidate index.html root paths (`foo/index.html` and `foo/`) (default: false) 
    };

    // create a new production publisher using S3 options 
    var publisher = awspublish.create({
        region: 'eu-central-1',
        params: {
            Bucket: 'web.staging.agombi.co.il'
        },
        accessKeyId: 'AKIAI55ZPQKNRGOFPIDQ',
        secretAccessKey: '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'
    }, {
        cacheFileName: 'staging-publish.cache'
    });

    return publish(publisher, cfSettings);
});



gulp.task('beta:web', function(){
    runSequence('bump', 'default', 'publish:staging');
});

gulp.task('release:web', function(){
    runSequence('bump', 'default', 'publish:production');
});


function publish(publisher, cfSettings) {
    // define custom headers 
    var headers = {
        'Cache-Control': 'no-cache, no-store, must-revalidate',
        'Expires': 0
            // ... 
    };


    return gulp.src(conf.paths.dist + '/**')
        .pipe(publisher.publish(headers))
        .pipe(cloudfront(cfSettings))
        .pipe(parallelize(publisher.publish(), 10))

    // create a cache file to speed up consecutive uploads 
    .pipe(publisher.cache())

    // print upload updates to console 
    .pipe(awspublish.reporter());
}
