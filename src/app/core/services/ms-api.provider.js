(function () {
    'use strict';

    angular
        .module('app.core')
        .provider('msApi', msApiProvider);

    /** @ngInject **/
    function msApiProvider() {
        /* ----------------- */
        /* Provider          */
        /* ----------------- */
        var provider = this;

        // Inject the $log service
        var $log = angular.injector(['ng']).get('$log');


        // Data
        var baseUrl = '';

        var accessToken = '';
        var devAccessToken = '';
        var api = [];

        // Methods
        provider.setBaseUrl = setBaseUrl;
        provider.getBaseUrl = getBaseUrl;
        provider.setAccessToken = setAccessToken;
        provider.setDevAccessToken = setDevAccessToken;
        provider.getAccessToken = getAccessToken;
        provider.getApiObject = getApiObject;
        provider.register = register;

        //////////

        /**
         * Set base url for API endpoints
         *
         * @param url {string}
         */
        function setBaseUrl(url) {
            baseUrl = url;
        }

        /**
         * Return the base url
         *
         * @returns {string}
         */
        function getBaseUrl() {
            return baseUrl;
        }


        /**
         * Set dev access token for API endpoints
         *
         * @param url {string}
         */
        function setDevAccessToken(accessTokenID) {
            devAccessToken = accessTokenID;
        }


        /**
         * Set access token for API endpoints
         *
         * @param url {string}
         */
        function setAccessToken(accessTokenID) {
            accessToken = accessTokenID;
        }


        /**
         * Return the access token for API endpoints
         *
         * @returns {string}
         */
        function getAccessToken() {
            return accessToken;
        }



        /**
         * Return the api object
         *
         * @returns {object}
         */
        function getApiObject() {
            return api;
        }

        /**
         * Register API endpoint
         *
         * @param key
         * @param resource
         */
        function register(key, resource) {
            if (!angular.isString(key)) {
                $log.error('"path" must be a string (eg. `dashboard.project`)');
                return;
            }

            if (!angular.isArray(resource)) {
                $log.error('"resource" must be an array and it must follow $resource definition');
                return;
            }

            var urlPath = (resource[0] || '');

            var url = function () {
                return urlPath;
            };


            // This patch let us use the old data structure of fuse template
            if (urlPath && urlPath.indexOf('app/data') === -1) {
                url = function () {
                    return getBaseUrl() + urlPath;
                };
            }

            var getAccessTokenForRequest = function(){
                if(urlPath.indexOf('serviceInterface') !== -1){
                    return devAccessToken;
                } else {
                    return accessToken;
                }
            };


            // Store the API object
            api[key] = {
                url          : url,
                accessToken  : getAccessTokenForRequest,
                paramDefaults: resource[1] || [],
                actions: resource[2] || [],
                options: resource[3] || {}
            };
        }

        /* ----------------- */
        /* Service           */
        /* ----------------- */
        this.$get = function ($log, $q, $resource, $rootScope, authService, $http, errorTrackService) {

            // Data

            // Methods
            var service = {
                setAccessToken: setAccessToken,
                getAccessToken: getAccessToken,
                setDevAccessToken: setDevAccessToken,
                register  : register,
                resolve   : resolve,
                request   : request
            };

            //Initialize
            //Set the accessToken
            service.setAccessToken(authService.getAccessToken());
            service.setDevAccessToken(authService.getDevAccessToken());

            return service;

            //////////

            /**
             * Resolve an API endpoint
             *
             * @param action {string}
             * @param parameters {object}
             * @returns {promise|boolean}
             */
            function resolve(action, parameters) {
                // Emit an event
                $rootScope.$broadcast('msApi::resolveStart');

                var actionParts = action.split('@'),
                    resource = actionParts[0],
                    method = actionParts[1],
                    params = parameters || {};


                if (!resource || !method) {
                    $log.error('msApi.resolve requires correct action parameter (resourceName@methodName)');
                    return false;
                }

                // Create a new deferred object
                var deferred = $q.defer();

                // Get the correct resource definition from api object
                var apiObject = api[resource];

                if (!apiObject) {
                    $log.error('Resource "' + resource + '" is not defined in the api service!');
                    deferred.reject('Resource "' + resource + '" is not defined in the api service!');
                }
                else {

                    params.access_token = apiObject.accessToken();  

                    // Generate the $resource object based on the stored API object
                    var resourceObject = $resource(apiObject.url(), apiObject.paramDefaults, apiObject.actions, apiObject.options);

                    // Make the call...
                    resourceObject[method](params,

                        // Success
                        function (response) {
                            deferred.resolve(response);

                            // Emit an event
                            $rootScope.$broadcast('msApi::resolveSuccess');
                        },

                        // Error
                        function (response) {
                            handleApiError(response);

                            deferred.reject(response);

                            // Emit an event
                            $rootScope.$broadcast('msApi::resolveError');
                        }
                    );
                }

                // Return the promise
                return deferred.promise;
            }

            /**
             * Make a request to an API endpoint
             *
             * @param action {string}
             * @param [parameters] {object}
             * @param [success] {function}
             * @param [error] {function}
             *
             * @returns {promise|boolean}
             */
            function request(action, parameters, success, error) {
                // Emit an event
                $rootScope.$broadcast('msApi::requestStart');

                var actionParts = action.split('@'),
                    resource = actionParts[0],
                    method = actionParts[1],
                    params = parameters || {};


                if ( !resource || !method )
                {
                    $log.error('msApi.resolve requires correct action parameter (resourceName@methodName)');
                    return false;
                }

                // Create a new deferred object
                var deferred = $q.defer();

                // Get the correct resource definition from api object
                var apiObject = api[resource];

                if (!apiObject) {
                    $log.error('Resource "' + resource + '" is not defined in the api service!');
                    deferred.reject('Resource "' + resource + '" is not defined in the api service!');
                }
                else {


                    params.access_token = apiObject.accessToken();



                    // Generate the $resource object based on the stored API object
                    var resourceObject = $resource(apiObject.url(), apiObject.paramDefaults, apiObject.actions, apiObject.options);


                    //Prepare the callbacks
                    // SUCCESS
                    var requestSuccessHandle = function (response) {
                        // Emit an event
                        $rootScope.$broadcast('msApi::requestSuccess');

                        // Resolve the promise
                        deferred.resolve(response);

                        // Call the success function if there is one
                        if (angular.isDefined(success) && angular.isFunction(success)) {
                            success(response);
                        }
                    };


                    // ERROR
                    var requestErrorHandle = function (response) {
                        handleApiError(response);

                        // Emit an event
                        $rootScope.$broadcast('msApi::requestError');

                        // Reject the promise
                        deferred.reject(response);

                        // Call the error function if there is one
                        if (angular.isDefined(error) && angular.isFunction(error)) {
                            error(response);
                        }
                    };


                    //Check if the method is my custom multipart
                    if (method === 'multipartPost' || method === 'multipartPut') {

                        var fd = new FormData();

                        Object.keys(params).forEach(function (key) {
                            var value = params[key];

                            //In case of array I use JSON strigify
                            if (value.constructor === Array || value.constructor === Object) {
                                fd.append(key, JSON.stringify(value));
                            } else {
                                fd.append(key, value);
                            }
                        });
                        if (method === 'multipartPost') {
                            $http.post(apiObject.url(), fd, {
                                transformRequest: angular.identity,
                                headers: { 'Content-Type': undefined }
                            })
                                .success(requestSuccessHandle)
                                .error(requestErrorHandle);
                        }
                        else if (method === 'multipartPut') {
                            $http.put(apiObject.url(), fd, {
                                transformRequest: angular.identity,
                                headers: { 'Content-Type': undefined }
                            })
                                .success(requestSuccessHandle)
                                .error(requestErrorHandle);
                        }
                        
                       
                        return;
                    }

                    // Make the call...
                    resourceObject[method](params, requestSuccessHandle, requestErrorHandle);
                }


                // Return the promise
                return deferred.promise;
            }


            //Hande API errors
            function handleApiError(error) {
                switch(error.status){
                    case 401:
                        break;
                    default:
                        errorTrackService.notify(error);
                        break;
                }
            }
        };
    }
})();