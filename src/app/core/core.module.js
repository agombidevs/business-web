(function ()
{
    'use strict';

    angular
        .module('app.core',
            [
                'ngAnimate',
                'ngAria',
                'ngCookies',
                'ngMessages',
                'ngResource',
                'ngSanitize',
                'ngMaterial',
                'lfNgMdFileInput',
                'thatisuday.ng-image-gallery',
                'pascalprecht.translate',
                'ui.router'
            ]);
})();