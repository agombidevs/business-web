(function() {
    'use strict';

    angular
        .module('app.core')
        .directive('msCard', msCardDirective)
        .controller('msCardController', MsCardController)
        .controller('dialogController', DialogController);

    /** @ngInject */
    function msCardDirective() {
        return {
            restrict: 'E',
            scope: {
                templatePath: '=template',
                card: '=ngModel',
                vm: '=viewModel'
            },
            template: '<div class="ms-card-content-wrapper" ng-include="templatePath" onload="cardTemplateLoaded()"></div>',
            compile: function(tElement) {
                // Add class
                tElement.addClass('ms-card');

                return function postLink(scope, iElement) {
                    // Methods
                    scope.cardTemplateLoaded = cardTemplateLoaded;

                    //////////

                    /**
                     * Emit cardTemplateLoaded event
                     */
                    function cardTemplateLoaded() {
                        scope.$emit('msCard::cardTemplateLoaded', iElement);
                    }
                };
            },


        };
    }

    function MsCardController($mdDialog, $scope, $document,PubSub) {
        var vm = this;

        function _changeImageSrc(img) {
            $scope.card.picture.thumb.source = img[0].lfDataUrl;
            $scope.card.file = img[0].lfFile;

        }
        //////////////PRIVATE/////////////
        function _showAdvanced(card) {
            //Ignore if not in edit mode
            if (card.edit !== true) {
                $mdDialog.show({
                    controller: DialogController,
                    templateUrl: 'app/core/directives/ms-card/templates/template-9-copy/dialog1.tmpl.html',
                    parent: angular.element($document.find('#foldersView')),
                    targetEvent: card,
                    clickOutsideToClose: true,
                    fullscreen: $scope.customFullscreen // Only for -xs, -sm breakpoints.
                })
                    .then(function(img) {
                        _changeImageSrc(img);
                    }, function() {
                    });
            }
        };
        
        $scope.$watch('folderForm.folder.$valid', function(validity) {
            PubSub.publish('folderFormValidate', {valid:validity});
        })

        ////////////PUBLIC///////////////  
        vm.showAdvanced = _showAdvanced;
    }

    function DialogController($scope, $mdDialog) {
        var vm = this;
        // $scope.$watch('vm.files', function (newVal, oldVal) {
        //     console.log(vm.files);
        // });
        $scope.hide = function() {
            $mdDialog.hide();
        };
        $scope.cancel = function() {
            $mdDialog.cancel();
        };
        $scope.answer = function(images) {
            //check the image size
            function getMeta(url, callback) {
                var img = new Image();
                img.src = url;
                img.onload = function() { callback(this.width, this.height); };
            }
            getMeta(
                images[0].lfDataUrl,
                function(width, height) { 
                    if(width<250 || height <250){
                    alert('קובץ קטן מידי');
                    }
                    else{
                        $mdDialog.hide(images);
                    }
                } 
            
            );
        };


    }
})();