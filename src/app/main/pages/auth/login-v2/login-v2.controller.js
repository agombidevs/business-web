(function() {
    'use strict';

    angular
        .module('app.pages.auth.login-v2')
        .controller('LoginV2Controller', LoginV2Controller);

    /** @ngInject */
    function LoginV2Controller($log, msApi, authService, $state) {
        var vm = this;

        vm.isProgress = false;
        vm.remoteError = null;

        //Default form values
        vm.form = {
            remember: true
        };

        // Data

        // Methods
        vm.login = login;
        //////////

        function login(form) {
            var password = form.password;
                var remember = form.remember;
                var creds ={};
            if (form.userNameEmail.indexOf('@') > 0) {
                
                var email = form.userNameEmail;
                 creds = { email: email, password: password, isLongSession: remember };

            } else {
                var userName = form.userNameEmail;
                 creds = { username: userName, password: password, isLongSession: remember };

            }

            vm.isProgress = true;
            msApi.request('login@save', creds,
                // SUCCESS
                function(response) {
                    if (!response.data.isAdmin) {
                        authService.setAccessToken(response.data.id, remember);
                        $state.go('app.chat');
                    } else {
                        authService.setDevAccessToken(response.data.devAccessToken);
                        $state.go('app.admin');
                    }
                },
                // ERROR
                function(response) {
                    vm.isProgress = false;
                    if (response.status === 401) {
                        vm.remoteError = 'הסיסמה שהזנת שגויה';
                    }
                }
            );
        }
    }
})();
