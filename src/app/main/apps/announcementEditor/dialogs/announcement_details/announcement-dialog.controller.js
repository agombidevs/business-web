(function ()
{
    'use strict';

    angular
    .module('app.announcementEditor')
    .controller('AnnouncementDetailsDialogController', AnnouncementDetailsDialogController);

    /** @ngInject */
    function AnnouncementDetailsDialogController($mdDialog, Announcement, msUtils, AnnouncementsServices)
    {
        var vm = this;

        // Data
        vm.title = 'פרטי עדכון';
        vm.announcement = angular.copy(Announcement);
        vm.newAnnouncement = false;
        vm.allFields = false;
        vm.isSaving = false;

        // Methods
        vm.closeDialog = closeDialog;
        vm.toggleInArray = msUtils.toggleInArray;
        vm.exists = msUtils.exists;

        vm.deleteAnnouncement = deleteAnnouncement;
        vm.saveAnnouncement = saveAnnouncement;
        vm.changePicture = changePicture;

        function deleteAnnouncement(ev) {
            //prepare the message format
            var confirm = $mdDialog.confirm()
            .title('האם אתה בטוח שברצונך למחוק?')
            .parent(angular.element(document.querySelector('#foldersView')))
            .targetEvent(ev)
            .clickOutsideToClose(true)
            .ok('מאשר')
            .cancel('ביטול');
            //open dialog
            $mdDialog.show(confirm).then(function() {

                if (vm.announcement.id === undefined) {
                    return;
                    //console.log('invalid id');
                } else {
                    AnnouncementsServices.deleteAnnouncements([vm.announcement.id]);
                // SUCCESS    
            }});}


            function saveAnnouncement(ev){
                vm.isSaving = true;
                AnnouncementsServices.updateAnnouncement(vm.announcement).then(function(response){
                    vm.isSaving = false;
                    closeDialog();
                }).catch(function(err){
                    vm.isSaving = false;
                });
            }

            function changePicture(){
  
            }
        /**
         * Close dialog
         */
         function closeDialog()
         {
            $mdDialog.hide();
        }

    }
})();