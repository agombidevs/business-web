(function ()
{
    'use strict';

    angular
        .module('app.announcementEditor',
            [
                // 3rd Party Dependencies
                'xeditable',
                'datatables',
                'flow'
            ]
        )
        .config(config)
        .run(runBlock);

    /** @ngInject */
    function config($stateProvider, $translatePartialLoaderProvider, msApiProvider)
    {

        $stateProvider.state('app.announcementEditor', {
            url    : '/announcementEditor',
            views  : {
                'content@app': {
                    templateUrl: 'app/main/apps/announcementEditor/announcementEditor.html',
                    controller : 'AnnouncementEditorController as vm'
                }
            },
            data: {
                permissions: {
                    only: 'AUTHORIZED',
                    redirectTo: 'app.pages_auth_login-v2'
                }
            }
        });


        //msApiProvider.register('customers', ['businessCustomers']);
        //msApiProvider.register('fields', ['businessFields']);

        msApiProvider.register('businessAnnouncements', ['businessAnnouncements']);



        // Translation
        $translatePartialLoaderProvider.addPart('app/main/apps/announcementEditor');


    }


    /** @ngInject */
    function runBlock(DTDefaultOptions)
    {
        DTDefaultOptions.setDOM('<"top"f>rt<"bottom"<"left"<"info"i><"pagination"p>><"right"<"length"l>>>');


    }

})();