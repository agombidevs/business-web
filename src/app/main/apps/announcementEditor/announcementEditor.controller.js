(function() {
    'use strict';

    angular
        .module('app.announcementEditor')
        .controller('AnnouncementEditorController', AnnouncementEditorController);

    /** @ngInject */
    function AnnouncementEditorController($mdSidenav, msApi, msUtils, $mdDialog, $timeout, $document, $log, AnnouncementsServices, DTOptionsBuilder, $scope, DTColumnDefBuilder, $q) {
        var vm = this;



        // Data
        vm.whereFilter = {};
        vm.filterIds = null;
        vm.listType = 'all';
        vm.listOrder = 'name';
        vm.listOrderAsc = false;
        vm.selected = {};
        vm.selectAll = false;
        vm.isLoading = true;

        // Methods
        vm.openAnnouncementDetailsDialog = openAnnouncementDetailsDialog;
        vm.toggleAll = toggleAll;
        vm.toggleOne = toggleOne;
        vm.selectedIdsArray = selectedIdsArray;
        vm.newAnnouncement = newAnnouncement;
        vm.deleteAnnouncement = deleteAnnouncement;
        vm.changeAnnouncementImage = changeAnnouncementImage;
        //////////


        getAllAnnouncements();
        initDataTable();

        function getAllAnnouncements() {
            $log.debug('getAllAnnouncements()');
            AnnouncementsServices.getBusinessaAnnouncements().then(function sucsses(res) {
                $log.debug('AnnouncementsServices.getBusinessaAnnouncements() sucsses');
                vm.announcements = AnnouncementsServices.model.AnnouncementsItems;

                vm.isLoading = !AnnouncementsServices.model.initialized;
                
            }, function(error) {
                ///TODO : handle error result
                vm.isLoading = !AnnouncementsServices.model.initialized;

                $log.error('galleryService.getBusinessGallery() error: ' + error);
            });
        }

        function initDataTable() {

            //Build the datatable options
            vm.dtOptions = DTOptionsBuilder.newOptions()
                .withOption('responsive', true)
                .withPaginationType('simple')
                .withOption(
                    'autoWidth', false)
                .withOption('destroy', true)
                .withLanguage({
                    'sEmptyTable': 'לא נמצאו עדכונים',
                    'sInfo': 'מציג  _END_-_START_  מתוך _TOTAL_ עדכונים',
                    'sInfoEmpty': 'לא נמצאו עדכונים',
                    'sInfoFiltered': '(סונן מתוך _MAX_ סך עדכונים)',
                    'sInfoPostFix': '',
                    'sInfoThousands': ',',
                    'sLengthMenu': 'הצג _MENU_ עדכונים',
                    'sLoadingRecords': 'טוען...',
                    'sProcessing': 'Processing...',
                    'sSearch': 'חפש:',
                    'sZeroRecords': 'No matching records found',
                    'oPaginate': {
                        'sFirst': 'ראשון',
                        'sLast': 'אחרון',
                        'sNext': 'הבא',
                        'sPrevious': 'הקודם'
                    },
                    'oAria': {
                        'sSortAscending': ': activate to sort column ascending',
                        'sSortDescending': ': activate to sort column descending'
                    }
                });

            //Build the columns of the table
            buildDataTableColumns();
        }


        function newAnnouncement(ev) {
            $mdDialog.show({
                controller         : 'NewAnnouncementDialogController',
                controllerAs       : 'vm',
                templateUrl        : 'app/main/apps/customers/dialogs/new-announcement/new-announcement-dialog.html',
                parent             : angular.element($document.find('#main')),
                targetEvent        : ev,
                clickOutsideToClose: true,
                locals             : {
                    visibilityType: 'anyone',
                    showFutureUsersCheckbox: false,
                    customerFilter: {},
                    CustomerIds: []
                }
            });
        }


        function deleteAnnouncement(ev) {
            //prepare the message format

            var confirm = $mdDialog.confirm()
                .title('האם אתה בטוח שברצונך למחוק?')
                .parent(angular.element(document.querySelector('#foldersView')))
                .targetEvent(ev)
                .clickOutsideToClose(true)
                .ok('מאשר')
                .cancel('ביטול');
            //open dialog
            $mdDialog.show(confirm).then(function() {

                var announcementIds = Object.keys(vm.selected);

                AnnouncementsServices.deleteAnnouncements(announcementIds);

                vm.selected = {};

                // SUCCESS    
            });

        }

        function buildDataTableColumns() {
            vm.dtColumnDefs = [
                DTColumnDefBuilder.newColumnDef(0).withOption('width', '1px'),
                DTColumnDefBuilder.newColumnDef(1).withOption('sWidth', '20px'),
                DTColumnDefBuilder.newColumnDef(2).withOption('sWidth', '60px')
            ];

        }



        function isEmpty(obj) {
            for (var prop in obj) {
                if (obj.hasOwnProperty(prop)) {
                    return false;
                }
            }

            return true && JSON.stringify(obj) === JSON.stringify({});
        }

        function cleanNullFromObject(obj) {

            for (var key in obj) {
                if (obj.hasOwnProperty(key)) {
                    var value = obj[key];

                    if (typeof value === 'object') {
                        if (isEmpty(value)) {
                            delete obj[key];
                        } else {
                            cleanNullFromObject(value);
                        }
                    }

                    if (value == null || value === undefined || (Array.isArray(value) && value.length === 0)) {
                        delete obj[key];
                    }
                }
            }

        }

        function refinedWhereFilter() {
            var whereFilter = angular.copy(vm.whereFilter);

            delete whereFilter['fields.age'];

            for (var fieldKey in whereFilter) {
                if (whereFilter.hasOwnProperty(fieldKey)) {
                    var fieldFilter = whereFilter[fieldKey];
                    if (fieldFilter.gte && fieldFilter.lte) {
                        //Merge the gte and lte to be in AND logic

                        if (!whereFilter.and) {
                            whereFilter.and = [];
                        }
                        var gteFilter = {};
                        gteFilter[fieldKey] = {
                            gte: fieldFilter.gte
                        };
                        whereFilter.and.push(gteFilter);
                        // Delete the old filter method
                        delete fieldFilter.gte;

                        var lteFilter = {};
                        lteFilter[fieldKey] = {
                            lte: fieldFilter.lte
                        };
                        whereFilter.and.push(lteFilter);
                        // Delete the old filter method
                        delete fieldFilter.lte;
                    }
                }
            }

            //Clean nulls in filter (We do it twice because we dont want empty object also)
            cleanNullFromObject(whereFilter);
            cleanNullFromObject(whereFilter);

            return whereFilter;
        }


        function toggleAll() {
            vm.selectAll = !vm.selectAll;
            if (vm.selectAll) {
                vm.announcements.forEach(function(announcement) {
                    vm.selected[announcement.id] = true;
                });
            } else {
                vm.selected = {};
            }
        }

        function toggleOne(announcementId) {
            var isSelected = vm.selected[announcementId];
            if (isSelected) {
                delete vm.selected[announcementId];
            } else {
                vm.selected[announcementId] = true;
            }
            var selectedCount = Object.keys(vm.selected).length;
            if (selectedCount === vm.announcements.length) {
                vm.selectAll = true;
            } else {
                vm.selectAll = false;
            }
        }

        function selectedIdsArray() {
            return Object.keys(vm.selected);
        }


        function openAnnouncementDetailsDialog(announcement) {
            $mdDialog.show({
                controller: 'AnnouncementDetailsDialogController',
                controllerAs: 'vm',
                templateUrl: 'app/main/apps/announcementEditor/dialogs/announcement_details/announcement-dialog.html',
                parent: angular.element($document.find('#main')),
                clickOutsideToClose: true,
                locals: {
                    Announcement: announcement,
                }
            });
        }

        function changeAnnouncementImage(targetAnnouncement) {
            $log.debug(targetAnnouncement);
            //Create blob and after that file with default image(prepare sending to server )
            var img = '../assets/images/logos/6.jpg';
            var file = null;

            $mdDialog.show({
                    controller: ChangeImageDialog,
                    templateUrl: 'app/main/apps/announcementEditor/dialogs/changeImageDialog/change-image-dialog.html',
                    targetEvent: targetAnnouncement,
                    focusOnOpen: true,
                    clickOutsideToClose: true,
                    escapeToClose: true
                })
                .then(function sucsses(img) {
                    if (img !== null) {
                        targetAnnouncement.file = img[0].lfFile;
                        AnnouncementsServices.updateAnnouncement(targetAnnouncement);

                    }
                }, function(error) {
                    $log.debug('changeAnnouncementImage() at $mdDialog', error);

                });

        }

        function ChangeImageDialog($scope, $mdDialog) {
            var vm = this;
            // $scope.$watch('vm.files', function (newVal, oldVal) {
            //     console.log(vm.files);
            // });
            $scope.hide = function() {
                $mdDialog.hide();

            };
            $scope.cancel = function() {
                $mdDialog.hide(null);


            };
            $scope.answer = function(images) {
                //check the image size


                function getMeta(url, callback) {
                    var img = new Image();
                    img.src = url;
                    img.onload = function() { callback(this.width, this.height); };

                }
                getMeta(
                    images[0].lfDataUrl,
                    function(width, height) {
                        if (width < 250 || height < 250) {
                            alert('קובץ קטן מידי');
                        } else {
                            $mdDialog.hide(images);
                        }
                    }
                );
            };


        }




        /**
         * Toggle sidenav
         *
         * @param sidenavId
         */
        function toggleSidenav(sidenavId) {
            $mdSidenav(sidenavId).toggle();
        }

    }

})();
