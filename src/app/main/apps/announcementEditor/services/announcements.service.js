'use strict';
(function() {
    angular.module('app.announcementEditor')
    .config(config)
    .factory('AnnouncementsServices', AnnouncementsServices);

    /** @ngInject */
    function config(msApiProvider) {
        // Agombi Api
        msApiProvider.register('businessAnnouncements', ['businessAnnouncements']);


    }

    function AnnouncementsServices($q, msApi, $log) {

        var _model = {
            initialized: false,
            loading: true,
            AnnouncementsItems: null,
            announcementsToDelete: [],
            isDeleting: false
        };

        /****************** PRIVATE *******************/

        /**
        * Delete item  on server
        * @param  
        * @returns {*}
        */
        function _deleteAnnouncements(announcementIds) {
            for (var i = 0; i < announcementIds.length; i++) {
                _model.announcementsToDelete.push(announcementIds[i]);
            }

            _excecuteDelete();
        }    


        function _excecuteDelete(){
            if(_model.announcementsToDelete.length === 0){
                _model.isDeleting = false;
                return;
            } 

            if(_model.isDeleting){
                return;
            }

            _model.isDeleting = true;

            var announcementId = _model.announcementsToDelete.pop();

            msApi.request('businessAnnouncements@delete', { id: announcementId},
                // SUCCESS
                function(response) {
                    var index = _.indexOf(_model.AnnouncementsItems, _.find(_model.AnnouncementsItems, { id: announcementId }));
                    _model.AnnouncementsItems.splice(index, 1);
                    _model.isDeleting = false;
                    _excecuteDelete();
             },
                // ERROR
                function(error) {
                    console.log(error);
                });
        }

        /**
        * Update or create item on server
        * @param  
        * @returns {*}
        */

        

        /**
        * Get business gallery items server
        * @param  
        * @returns {*}
        */

        function _getBusinessaAnnouncements() {
            var deferred = $q.defer();
            msApi.request('businessAnnouncements@get',{},
                // SUCCESS
                function(response) {
                    if (response.data) {
                        _model.AnnouncementsItems = JSON.parse(JSON.stringify(response.data));
                        deferred.resolve(response.data);
                        _model.initialized = true;
                    }
                },
                // ERROR
                function(error) {
                                            _model.initialized = false;

                    deferred.resolve(error);
                    console.log(error);
                }
                );
            return deferred.promise;
        }

        function GetBusinessAnnouncements() {
            var deferred = $q.defer();
            msApi.request('businessAnnouncements@get', {},
                // SUCCESS
                function(response) {
                    if (response.data) {
                        _model.loading = false;
                        _model.BusinessGallery = JSON.parse(JSON.stringify(response.data));
                        _model.initialized = true;
                        //Create 'file' property (prepare for send after edit)
                        _.forEach(_model.BusinessGallery, function(value, key) {
                            value.edit = true;
                            //Create 'file' property (prepare for send after edit)
                            if (value.file === undefined || value.file === null) {
                                var img = value.picture.thumb.source;
                                var file = null;
                                blobUtil.imgSrcToBlob(img, 'image/jpeg', null, 1.0).then(function(blob) {
                                    file = blob;
                                    file.lastModifiedDate = new Date();
                                    file.name = value.title;
                                    value.file = file;
                                    return value;
                                }).catch(function(err) {
                                    $log.debug("blobUtil err" + err);
                                });
                            }
                        });
                        deferred.resolve(true);
                    }
                },
                // ERROR
                function(error) {
                    deferred.resolve(error);
                    _model.initialized = false;
                    console.log(error);
                }
                );
            return deferred.promise;
        }
        /**
         * Create business collection folder on server
         * @param folder 
         * @returns {*}
         */
         function _updateAnnouncement(announcement) {
            var deferred = $q.defer();
            var announcementObject = {};
            if(!announcement.id){
                return null;
            }
            announcementObject.id = announcement.id;
            announcementObject.title = announcement.title;
            announcementObject.content = announcement.content;
            if(announcement.file){
                announcementObject.file = announcement.file;
            }
            msApi.request('businessAnnouncements@multipartPost',announcementObject,
                // SUCCESS
                function(response) {
                    if (response) {
                        var res = JSON.parse(JSON.stringify(response));
                        var index = _.indexOf(_model.AnnouncementsItems, _.find(_model.AnnouncementsItems, { id: response.id }));
                        if (index > -1) {
                            res.edit = true;
                            _model.AnnouncementsItems.splice(index, 1, res);
                            deferred.resolve(response);
                        } else {
                            res.edit = true;
                            _model.AnnouncementsItems.push(res);
                            deferred.resolve(response);
                        }


                //console.log(response);

                    }
                },
                // ERROR
                function(error) {
                    deferred.reject(error);
                    console.log(error);
                }
                );

            return deferred.promise;
        }
        /**
         * Delete business collection folder on server
         * @param folder id
         * @returns {*}
         */



        // function init() {
        //     _getBusinessGallery();
        // }

        // init();
        /****************** PUBLIC *******************/

        var service = {
            get model() { return _model; },
            set model(m) { _model = m; },
            getBusinessaAnnouncements: _getBusinessaAnnouncements,
            updateAnnouncement: _updateAnnouncement,
            deleteAnnouncements: _deleteAnnouncements
        };

        return service;
    }


})();