(function ()
{
    'use strict';

    angular
        .module('app.dashboards.analytics')
        .controller('DashboardAnalyticsController', DashboardAnalyticsController);

    /** @ngInject */
    function DashboardAnalyticsController(msApi)
    {
        var vm = this;


        vm.colors = ['blue-bg', 'blue-grey-bg', 'orange-bg', 'pink-bg', 'purple-bg'];


        initController();

        function initController(){
            msApi.resolve('analytics.installations@get').then(function(response){
                vm.installationsChart = response;

                vm.charts = [{
                    key: 'התקנות',
                    values: vm.installationsChart.data
                }];

                vm.widget1 = {
                    title             : 'כמות הורדות',
                    totalInstallations       : vm.installationsChart.total,
                    bigChart          : {
                        options: {
                            chart: {
                                type                   : 'lineWithFocusChart',
                                color                  : ['#2196F3'],
                                height                 : 400,
                                margin                 : {
                                    top   : 32,
                                    right : 32,
                                    bottom: 64,
                                    left  : 48
                                },
                                isArea                 : true,
                                useInteractiveGuideline: true,
                                duration               : 1,
                                clipEdge               : true,
                                clipVoronoi            : false,
                                showLegend             : false,
                                x                      : function (d)
                                {
                                    return d.x;
                                },
                                y                      : function (d)
                                {
                                    return d.y;
                                },
                                xAxis                  : {
                                    showMaxMin: false,
                                    tickFormat: function (d)
                                    {
                                        var date = new Date(d);
                                        return d3.time.format('%b %d')(date);
                                    }
                                },
                                yAxis                  : {
                                    showMaxMin: false
                                },
                                x2Axis                 : {
                                    showMaxMin: false,
                                    tickFormat: function (d)
                                    {
                                        var date = new Date(d);
                                        return d3.time.format('%b %d')(date);
                                    }
                                },
                                y2Axis                 : {
                                    showMaxMin: false
                                },
                                interactiveLayer       : {
                                    tooltip: {
                                        gravity: 's',
                                        classes: 'gravity-s'
                                    }
                                },
                                legend                 : {
                                    margin    : {
                                        top   : 8,
                                        right : 0,
                                        bottom: 32,
                                        left  : 0
                                    },
                                    rightAlign: true
                                }
                            }
                        },
                        data   : vm.charts
                    }
                };

            });
        }

    

        // Methods

        //////////


    }

})();