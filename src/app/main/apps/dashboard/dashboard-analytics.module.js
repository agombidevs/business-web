(function ()
{
    'use strict';

    angular
        .module('app.dashboards.analytics',
            [
                // 3rd Party Dependencies
                'nvd3'
            ]
        )
        .config(config);

    /** @ngInject */
    function config($stateProvider, msApiProvider)
    {
        // State
        $stateProvider.state('app.dashboards_analytics', {
            url      : '/dashboard-analytics',
            views    : {
                'content@app': {
                    templateUrl: 'app/main/apps/dashboard/dashboard-analytics.html',
                    controller : 'DashboardAnalyticsController as vm'
                }
            },
            bodyClass: 'dashboard-analytics'
        });

        msApiProvider.register('analytics.installations', ['businessCustomers/installations/chart']);
    }

})();