(function ()
{
    'use strict';

    angular
        .module('app.chat')
        .config(config)
        .factory('chatService', chatService);



    /** @ngInject */
    function config(msApiProvider) {

        // Agombi Api
        msApiProvider.register('customers', ['businessCustomers']);
        msApiProvider.register('chatCredentials', ['businessUsers/chatCredentials']);
        msApiProvider.register('businessesOwnDetails', ['businessUsers/me']);
        msApiProvider.register('offlineMessagesCount', ['businessUsers/offlineMessagesCount']);

    }




    /** @ngInject */
    function chatService($q, PubSub, msApi, envService, _, errorTrackService, msNavigationService, authService, $state, $rootScope, $localStorage)
    {
        var kMaxReconnectRetries = 100;
        var kReconnectTimeout = 3 * 1000;  // in ms
        var kUnreadMessagesKey = 'unreadMessages';

        var isChatConnected = false;

        var reconnectRetriesLeft = kMaxReconnectRetries;
        var autoReconnect = true;
        var stropheLib = Strophe;
        var pres = $pres;

        var activeCustomer;

        var chatAddress;
        var chatPassword;


        var service = {
            chats         : {},
            customers      : undefined,
            setActiveCustomer: _setActiveCustomer,
            onConnected: _onConnected,
            setOnlineStatus: _setOnlineStatus,
            setOfflineStatus: _setOfflineStatus,
            sendTextMessage: _sendTextMessage,
            getCustomerChat: _getCustomerChat,
            getHistoryMessages: _getHistoryMessages,
            getCustomers: _getCustomers,
            getBusinessesDetails: _getBusinessesDetails,
            isOnline: true
        };


        var ignoredErrors = ['Websocket closed unexpectedly', 'Error: host-unknown'];

        // LogLevel: {
        //     DEBUG: 0,
        //     INFO: 1,
        //     WARN: 2,
        //     ERROR: 3,
        //     FATAL: 4
        // }
        var minLogLevel = stropheLib.LogLevel.ERROR;

        stropheLib.log = function (level, msg) {
            if(minLogLevel <= level){
                console.log(msg);
            }
            // if(level >= stropheLib.LogLevel.ERROR){
            //     if(!_.contains(ignoredErrors, msg)){
            //         errorTrackService.notify(msg);
            //     }
            // }
        };


        // Get the chat URL depends on the environment (staging or production)
        var connectionString = envService.read('webSocketChatUrl');

        //Initialize the connection variable
        var connection;


        if(authService.isAuthorized()){
           _initChatService(); 
        }

        authService.addAuthorizedHandler(_initChatService);
        authService.addUnAuthorizedHandler(_endChatService);






        /****************** PRIVATE *******************/


        function _initChatService() {

            _enableAutoReconnect();

            //Start the authentication
            _getCustomers().then(function(){
                _connect();
                _updateUnseenMessagesCount();
            });
        }


        function _endChatService() {

            _disableAutoReconnect();
            
            connection.flush();
            connection.options.sync = true;    
            connection.disconnect();   
        }

            

        function _onConnected(callback) {
            if(isChatConnected){
                callback();
            } else {
                var token = PubSub.subscribe('chatDidConnected', function(){
                    PubSub.unsubscribe(token);
                    setTimeout(function(){
                        callback();  
                    },200);
                });
            }
        }


        function _setOnlineStatus() {
            connection.send(pres());
            service.isOnline = true;
        }


        function _setOfflineStatus() {
            var offlinePresence = pres({
                    xmlns: stropheLib.NS.CLIENT,
                    type: 'unavailable'
                });
            connection.send(offlinePresence);
            service.isOnline = false;
        }


        function _enableAutoReconnect() {
            reconnectRetriesLeft =  kMaxReconnectRetries;
            autoReconnect = true;
        }


        function _disableAutoReconnect() {
            autoReconnect = false;
        }


        function _connect() {
            //Fetch chat credentials first if doesnt exist
            if(!chatAddress || !chatPassword){
                msApi.request('chatCredentials@get', {},
                    //Success
                    function (response) {
                        chatAddress = response.data.address;
                        chatPassword = response.data.password;     

                        _connect();    
                    },

                    //Error
                    function (response) {
                        console.log(response);
                    }
                );

                return;
            }
            
            /**
             * connection by strophe
             */
            //Initialize the connection variable
            connection = new stropheLib.Connection(connectionString, {protocol: 'ws'});


            connection.connect(chatAddress, chatPassword, function (status, error) {

                if(error){
                    console.log('Strophe connection error: '+error);
                }

                switch(status) {
                    case stropheLib.Status.CONNECTING:
                        console.log('Strophe is connecting.');
                        break;
                    case stropheLib.Status.AUTHFAIL:
                        console.log('Chat authentication failed with credentials: '+chatAddress+':'+chatPassword);
                        break;
                    case stropheLib.Status.CONNECTED:
                        console.log('Strophe is connected.');
                        isChatConnected = true;

                        //Add internal handler for chat messages
                        connection.addHandler(_onChatMessage, null, 'message', 'chat', null, null);

                        connection.messageCarbons.enable();

                        if(service.isOnline){
                            _setOnlineStatus();
                        } else {
                            _setOfflineStatus();
                        }

                        PubSub.publish('chatDidConnected');

                        break; 
                    case stropheLib.Status.DISCONNECTING:
                        console.log('Strophe is disconnecting.');
                        isChatConnected = false;
                        break;
                    case stropheLib.Status.DISCONNECTED:
                        console.log('Strophe is disconnected.');
                        isChatConnected = false;
                        if(reconnectRetriesLeft > 0 && autoReconnect){
                            setTimeout(function(){
                                _connect(); 
                            }, kReconnectTimeout);

                            reconnectRetriesLeft--;
                        }
                        break;
                    default:
                        console.log('Unknown Strophe Connection Status: '+status);
                }

            });

        }


        function _getCustomerIdFromMessage(message){
            if(message.from === connection.authzid){
                return stropheLib.getNodeFromJid(message.to);
            } else {
                return stropheLib.getNodeFromJid(message.from);
            }
        }


        function _onChatMessage(xmlMessage) {
            var fwd = xmlMessage.querySelector('forwarded');
            if(fwd){
                xmlMessage = fwd.querySelector('message');
            }

            var message = _xmlMessageToMessage(xmlMessage);

            if(!message.text){
                return true;
            }

            var customerId = _getCustomerIdFromMessage(message);
            var customer = service.customers.getById(customerId);

            //Check if the customer exist in the customers list
            //If not so maybe it is new customer so it will request the customer from the API
            if(!customer){
                _getCustomer(customerId).then(function(){
                    //Proccess the message again after the customer loaded
                    _onChatMessage(xmlMessage);
                });
                return true;
            } 


            var chat = _getCustomerChat(customerId);

            chat.push(message);

            _updateLastChatMessage(customerId);
            

            var data = {
                customerId: customerId,
                message: message
            };


            //Notify about the new message to handlers
            PubSub.publish('onChatMessage', data);

            //Check if the current customer is active 
            //And if not it will and +1 to unread
            if(activeCustomer !== customerId){
                _increaseCustomerUnseenMessagesCount(customerId ,1);

                _.defer(function() {
                    $rootScope.$apply();
                });          
            }


            // we must return true to keep the handler alive.  
            // returning false would remove it after it finishes.
            return true;
        }


        function _updateUnseenMessagesCount() {
            var messagesCount = 0;
            var unreadMessagesDict = _getUnreadMessages();

            for (var customerId in unreadMessagesDict){
                if (unreadMessagesDict.hasOwnProperty(customerId)) {

                    //Update customer object
                    var customer = service.customers.getById(customerId);
                    if(!customer){
                        delete unreadMessagesDict[customerId];
                        continue;
                    }
                    customer.unread = unreadMessagesDict[customerId];
                    messagesCount += unreadMessagesDict[customerId];
                }
            }            


            if(messagesCount === 0){
                delete msNavigationService.getNavigation('Chat')[0].badge;
            } else {
                var appBadge = {
                    content: messagesCount,
                    color: '#5F5F5F'
                };
                msNavigationService.getNavigation('Chat')[0].badge = appBadge;   
            }
        }

        function _getUnreadMessages(){
            if(!$localStorage[kUnreadMessagesKey]){
                $localStorage[kUnreadMessagesKey] = {};
            }
            return $localStorage[kUnreadMessagesKey];
        }

        function _increaseCustomerUnseenMessagesCount(customerId, messagesCount) {
            var unreadMessages = _getUnreadMessages();
            if(!unreadMessages[customerId]){
                unreadMessages[customerId] = 0;
            }
            unreadMessages[customerId] += messagesCount;

            _updateUnseenMessagesCount();
        }


        function _sendTextMessage(text, customerId){
            var toAddress = customerId + '@' + connection.domain;
            var id = connection.getUniqueId();
            var messageDate = new Date();
            var xmlMessage = $msg({
                id: id,
                to: toAddress,
                from: connection.authzid,
                type: 'chat',
                date: messageDate
            }).c('body').t(text);

            //Send the message
            connection.send(xmlMessage);

            //Create the message for local usage
            var message = {
                who: 'user',
                text: text.decodeHTML(),
                date: messageDate.toISOString()
            };

            return message;
        }


        function _xmlMessageToMessage(xmlPacket, date) {
            if(!date){
                var delay = xmlPacket.querySelector('delay');
                if(delay){
                    date = new Date(delay.getAttribute('stamp'));
                } else if(xmlPacket.getAttribute('date')){
                    date = new Date(xmlPacket.getAttribute('date'));
                } else {
                    date = new Date();
                }
            } 

            var message = {
                id: xmlPacket.getAttribute('id'),
                with: stropheLib.getBareJidFromJid(xmlPacket.getAttribute('to')),
                date: date.toISOString(),
                from: stropheLib.getBareJidFromJid(xmlPacket.getAttribute('from')),
                to: stropheLib.getBareJidFromJid(xmlPacket.getAttribute('to')),
                type: xmlPacket.getAttribute('type'),
                body: xmlPacket.getAttribute('body'),
                text: stropheLib.getText(xmlPacket.getElementsByTagName('body')[0])
            };

            if(message.text){
                //Replace wrong chars 
                message.text = message.text.decodeHTML();     
            }


            //Get current user is from the connection
            var currentUserAddress = connection.authzid;

            if(currentUserAddress === message.from){
                message.who = 'user';
            } else {
                message.who = 'contact';
            }

            return message;
        }


        function _getBusinessesDetails() {
            var deferred = $q.defer();
            msApi.request('businessesOwnDetails@get', {},
                // SUCCESS
                function (response) {
                    deferred.resolve(response);
                },
                // ERROR
                function (error) {
                    deferred.resolve(error);
                    console.log(error);
                }
            );
            return deferred.promise;
        }

        function _updateLastChatMessage(customerId) {
            var customer = service.customers.getById(customerId);
            var customerChat = service.chats[customerId];
            if(customerChat.length > 0) {
                customer.lastChatMessage = customerChat[customerChat.length - 1];
            }
        }


        function _getHistoryMessages(customerId, max) {

            var deferred = $q.defer();

            _onConnected(function(){

                var contactXmppAddress = customerId + '@' + connection.domain;

                var end = (new Date()).toISOString();

                if(service.chats[customerId][0]){
                    var isoStringDate = service.chats[customerId][0].date;
                    var unixTime = (new Date(isoStringDate)).getTime();
                    unixTime = unixTime - 1;
                    var earlierStringDate = new Date(unixTime).toISOString();
                    end = earlierStringDate;
                }

                var messageCounter = 0;

                connection.mam.query(connection.authzid, {
                    'queryid': customerId,
                    'before': '',
                    'end': end,
                    'max': max,
                    'with': contactXmppAddress,
                    onMessage: function(xmlPacket) {
                        var result = xmlPacket.querySelector('result');
                        var finish = xmlPacket.querySelector('fin');
                        
                        if(finish){
                            customerId = finish.getAttribute('queryid');

                            //Change boolean if there no more history
                            if(messageCounter < max){
                                service.customers.getById(customerId).noMoreHistory = true;
                            }

                            _updateLastChatMessage(customerId);
                            deferred.resolve();
                            return false;
                        }
                        if(result){
                            customerId = result.getAttribute('queryid');

                            var from = $(xmlPacket).find('forwarded message').attr('from');

                            messageCounter++;

                            var fwd = xmlPacket.querySelector('forwarded');
                            var messageDate = new Date(fwd.querySelector('delay').getAttribute('stamp'));

                            // console.log(fwd.querySelector('delay').getAttribute('stamp'));

                            var xmlMessage = fwd.querySelector('message');

                            var message = _xmlMessageToMessage(xmlMessage, messageDate);


                            //Add the message to the correct chat
                            //In the correct place (index)
                            var customerChat = service.chats[customerId];

                            var messageIndex = 0;
                            while(messageIndex < customerChat.length && customerChat[messageIndex].date < message.date){
                                messageIndex++;
                            }
                            customerChat.splice(messageIndex, 0, message);

                            return true; 
                        }
                    },
                    onComplete: function(response) {
                        console.log('Got all the messages');
                        // console.log(response);
                    }
                });
            });
            

            return deferred.promise;
        }


        function _getCustomers() {

            var deferred = $q.defer();

            msApi.request('customers@get', {},

                // SUCCESS
                function (response) {
                    var customers = response.customers;
                    //Decode the lastChatMessage text
                    for (var i = 0; i < customers.length; i++) {
                        var customer = customers[i];
                        if(customer.lastChatMessage && 
                            customer.lastChatMessage.text){
                            customer.lastChatMessage.text = customer.lastChatMessage.text.decodeHTML();
                        }
                    }
                    service.customers = customers;
                    deferred.resolve(service.customers);

                },
                // ERROR
                function (error) {
                    deferred.resolve(error);
                    console.log(error);
                }
            );  

    

            return deferred.promise;
        }



        function _getCustomer(customerId) {

            var deferred = $q.defer();


            msApi.request('customers@get', {
                where: {
                    id: customerId
                }
            },

                // SUCCESS
                function (response) {
                    if(response.customers.length === 1){
                        var customer = response.customers[0];
                        service.customers.push(customer);
                        deferred.resolve(customer);
                    } else {
                        deferred.reject('customer not found');
                    }
                },
                // ERROR
                function (error) {
                    deferred.reject(error);
                    console.log(error);
                }
            );  

    

            return deferred.promise;
        }



        /**
         * Get contact chat from the server
         *
         * @param customerId
         * @returns {*}
         */
        function _getCustomerChat(customerId)
        {
            if ( !service.chats[customerId] )
            {        
                service.chats[customerId] = [];  
            }      

            return service.chats[customerId];
        }

        function _setActiveCustomer(customerId)
        {
            activeCustomer = customerId;

            if(customerId){
                var unreadMessages = _getUnreadMessages();

                unreadMessages[customerId] = 0;

                _updateUnseenMessagesCount();
            }
        }


        return service;

    }



    /**
     * Array prototype
     *
     * Get by id
     *
     * @param value
     * @returns {T}
     */
    Array.prototype.getById = function (value)
    {
        return this.filter(function (x)
        {
            return x.id === value;
        })[0];
    };


    String.prototype.encodeHTML = function() {
        return this.split(/&/g).join('&amp;')
            .split(/</g).join('&lt;')
            .split(/>/g).join('&gt;')
            .split(/"/g).join('&quot;')
            .split(/'/g).join('&apos;');
    };


    String.prototype.decodeHTML = function() {
        return Autolinker.link(emojione.toImage(this.split(/&apos;/g).join('\'')
            .split(/&quot;/g).join('"')
            .split(/&gt;/g).join('>')
            .split(/&lt;/g).join('<')
            .split(/&amp;/g).join('&')), {} );
    };




})();