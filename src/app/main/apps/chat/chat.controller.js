(function() {
    'use strict';




    angular
        .module('app.chat')
        .controller('ChatController', ChatController)
        .directive('upwardsScoll', upwardsScollDirective);



    /** @ngInject */
    function ChatController($log, $q, $mdSidenav, $timeout, $document, $mdMedia, chatService, $scope, _, PubSub, utilsService) {

        var kStartedHistoryLength = 20;

        var vm = this;

        vm.isChatConnected = false;

        // Data
        vm.customers = null;
        vm.user = null;
        vm.emojioneArea = null;
        vm.currentConversation = {
            customer: {}
        };

        vm.leftSidenavView = false;
        vm.chat = undefined;
        vm.isLoadingHistory = false;

        // Methods
        vm.loadConversation = loadConversation;
        vm.loadMoreMessages = loadMoreMessages;
        vm.toggleSidenav = toggleSidenav;
        vm.toggleLeftSidenavView = toggleLeftSidenavView;
        vm.reply = reply;
        vm.setUserStatus = setUserStatus;
        vm.clearMessages = clearMessages;
        vm.mobileAndTabletcheck = utilsService.mobileAndTabletcheck;
        vm.onMessageAreaFocus = onMessageAreaFocus;




        //Initialize the chat
        initChat();

        //////////

        /**
         * Get Chat by Customer id
         * @param customerId
         */
        function loadConversation(customerId) {
            chatService.setActiveCustomer(customerId);

            var customer = vm.customers.getById(customerId);
            vm.currentConversation.customer = customer;

            //Clear chats
            vm.chat = [];

            //Init the history parameters
            vm.isLoadingHistory = false;

            // Reset the reply textarea
            resetReplyTextarea();



            if (!$mdMedia('gt-md')) {
                $mdSidenav('left-sidenav').close();
            }

            // Reset Left Sidenav View
            vm.toggleLeftSidenavView(false);

            vm.chat = chatService.getCustomerChat(customerId);



            //Load history if messages count less than minimum
            if (vm.chat.length < kStartedHistoryLength && !customer.noMoreHistory) {
                vm.isLoadingHistory = true;
                chatService.getHistoryMessages(customer.id, kStartedHistoryLength).then(function() {
                    vm.isLoadingHistory = false;
                    scrollToBottomOfChat();
                    //Focus the message area
                    if (!vm.mobileAndTabletcheck()) {
                        document.getElementById('message-area').focus();
                    }
                });
            }

            // Scroll to the last message
            scrollToBottomOfChat();

            //Focus the message area
            if (!vm.mobileAndTabletcheck()) {
                document.getElementById('message-area').focus();
            }
        }


        function loadMoreMessages() {
            var deferred = $q.defer();

            if (vm.isLoadingHistory || vm.currentConversation.customer.noMoreHistory) {
                deferred.resolve();
            } else {
                vm.isLoadingHistory = true;
                chatService.getHistoryMessages(vm.currentConversation.customer.id, kStartedHistoryLength).then(function() {
                    vm.isLoadingHistory = false;
                    deferred.resolve();
                });
            }
            return deferred.promise;
        }



        function onMessage(data) {

            var customerId = data.customerId;
            var message = data.message;

            //Add this message to the lastChatMessage
            var customer = vm.customers.getById(customerId);

            if (!customer) {
                return;
            }

            // customer.lastChatMessage = message;

            //If the message belongs to current conversation
            if (customerId === vm.currentConversation.customer.id) {

                // vm.chat.push(message);
                scrollToBottomOfChat();
            }

            _.defer(function() {
                $scope.$apply();
            });
        }



        function onMessageAreaFocus(){
            scrollToBottomOfChat();
        }

        /**
         * Reply
         */
        function reply($event) {
            // If "shift + enter" pressed, grow the reply textarea
            if ($event && $event.keyCode === 13 && $event.shiftKey) {
                vm.textareaGrow = true;
                return;
            }

            // Prevent the reply() for key presses rather than the"enter" key.
            if ($event && ($event.keyCode !== 13 || vm.mobileAndTabletcheck())) {
                return;
            }

            //Remove the lines break in the start and the end of the string 
            vm.replyMessage = vm.replyMessage.replace(/^\s+|\s+$/g, '');

            // Check for empty messages
            if (vm.replyMessage === '') {
                resetReplyTextarea();
                return;
            }



            //Send the message through the service
            var message = chatService.sendTextMessage(vm.replyMessage, vm.currentConversation.customer.id);


            // Add the message to the chat
            vm.chat.push(message);

            // Update Contact's lastChatMessage
            vm.customers.getById(vm.currentConversation.customer.id).lastChatMessage = message;

            // Reset the reply textarea
            resetReplyTextarea();

            // Scroll to the new message
            scrollToBottomOfChat();

            // Make sure the message area stay focused
            document.getElementById('message-area').focus();
            _.defer(function() {
                $scope.$apply();
            });
        }

        /**
         * Clear Chat Messages
         */
        function clearMessages() {
            vm.customers.getById(vm.currentConversation.customer.id).lastChatMessage = null;
        }

        /**
         * Reset reply textarea
         */
        function resetReplyTextarea() {
            vm.replyMessage = '';
            if(vm.emojioneArea){
                vm.emojioneArea.setText('');
            }
            vm.textareaGrow = false;
        }

        // *
        //  * Scroll Chat Content to the bottom
        //  * @param speed

        function scrollToBottomOfChat() {
            $timeout(function() {
                var chatContent = angular.element($document.find('#chat-content'));

                chatContent.animate({
                    scrollTop: chatContent[0].scrollHeight
                }, 400);
            }, 0);

        }

        /**
         * Set User Status
         */
        function setUserStatus(status) {
            vm.user.status = status;
        }

        /**
         * Toggle sidenav
         *
         * @param sidenavId
         */
        function toggleSidenav(sidenavId) {
            $mdSidenav(sidenavId).toggle().then(function() {
                //Fix fab-button bug
                if (sidenavId === 'left-sidenav') {
                    $('#contacts-button').hide().show(0);

                    //If the sidenave open (it is mean the sidenav hiding the chat)
                    //It will temporary remove the active customer
                    if($mdSidenav(sidenavId).isOpen()){
                        chatService.setActiveCustomer(undefined);
                    } else {
                        chatService.setActiveCustomer(vm.currentConversation.customer.id);
                    }
                }
            });
        }

        /**
         * Toggle Left Sidenav View
         *
         * @param view id
         */
        function toggleLeftSidenavView(id) {
            vm.leftSidenavView = id;
        }



        function getOwnBussinseDetail() {
            var deferred = $q.defer();
            chatService.getBusinessesDetails().then(function success(response) {
                    vm.user = response;
                    deferred.resolve(response);
                },
                function failure(error) {
                    //TODO LOG
                    deferred.reject(error);
                });
            return deferred.promise;
        }


        function initData() {

            //Set customers
            vm.customers = chatService.customers;

            var deferred = $q.defer();

            $q.all([
                getOwnBussinseDetail()

            ]).then(
                function success(result) {
                    deferred.resolve(result);
                },
                function failure(error) {
                    $log.error(error);

                    deferred.reject(error);
                }
            );
            return deferred.promise;
        }

        function initChat() {

            // Enable emoji only on desktop 
            if(!vm.mobileAndTabletcheck()){
                $(document).ready(function() {
                    vm.emojioneArea = $('#message-area').emojioneArea()[0].emojioneArea;
                    vm.emojioneArea.on('keyup', function(element, event){
                        vm.replyMessage = vm.emojioneArea.getText();
                        reply(event);
                    });
                });
            }


            chatService.onConnected(function() {

                // Send connect status
                chatService.setOnlineStatus();
                vm.isChatConnected = true;

                initData().then(function() {
                    //Subscribe to chat message event
                    vm.onChatMessageToken = PubSub.subscribe('onChatMessage', onMessage);
                });
            });
        }



        $scope.$on('$destroy', function() {
            if (vm.onChatMessageToken) {
                PubSub.unsubscribe(vm.onChatMessageToken);
                chatService.setActiveCustomer(undefined);
            }
        });



        /**
         * Array prototype
         *
         * Get by id
         *
         * @param value
         * @returns {T}
         */
        Array.prototype.getById = function(value) {
            return this.filter(function(x) {
                return x.id === value;
            })[0];
        };
    }



    /** @ngInject */
    function upwardsScollDirective($timeout) {
        return {
            link: function(scope, elem, attr, ctrl) {
                var raw = elem[0];

                elem.bind('scroll', function() {
                    if (raw.scrollTop <= 0) {
                        var sh = raw.scrollHeight;
                        var loadFunction = scope.$eval(attr.upwardsScoll);
                        loadFunction().then(function() {
                            $timeout(function() {
                                scope.$apply(function() {
                                    elem.scrollTop(raw.scrollHeight - sh);
                                });
                            }, 0);
                        });
                    }
                });

                //scroll to bottom
                $timeout(function() {
                    scope.$apply(function() {
                        elem.scrollTop(raw.scrollHeight);
                    });
                }, 0);
            }
        };
    }

})();
