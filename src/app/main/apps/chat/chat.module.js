(function () {
    'use strict';

    angular
        .module('app.chat', [
            'zInfiniteScroll'
        ])
        .config(config);

    /** @ngInject */
    function config($stateProvider, $httpProvider, $translatePartialLoaderProvider, msApiProvider) {

        // State
        $stateProvider.state('app.chat', {
            url: '/chat',
            views: {
                'content@app': {
                    templateUrl: 'app/main/apps/chat/chat.html',
                    controller: 'ChatController as vm'
                }
            },
            data: {
                permissions: {
                    only: 'AUTHORIZED',
                    redirectTo: 'app.pages_auth_login-v2'
                }
            }
        });


        // Translation
        $translatePartialLoaderProvider.addPart('app/main/apps/chat');


    }    


})();