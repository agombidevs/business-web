'use strict';
(function() {
    angular.module('app.gallery')
        .config(config)
        .factory('galleryService', GalleryService);

    /** @ngInject */
    function config(msApiProvider) {
        // Agombi Api
        msApiProvider.register('businessGallery', ['businessGallery/collections']);
        msApiProvider.register('businessGalleryItems', ['businessGallery/items']);

    }

    function GalleryService($q, msApi, $log) {

        var _model = {
            initialized: false,
            loading: true,
            BusinessGallery: null,
            BsinessGalleryItems: null
        };

        /****************** PRIVATE *******************/

        /**
        * Delete item  on server
        * @param  
        * @returns {*}
        */
        function _deleteItem(itemId) {
            var deferred = $q.defer();

            msApi.request('businessGalleryItems@delete', { itemId: itemId },
                // SUCCESS
                function(response) {
                    if (response) {
                        var index = _.indexOf(_model.BsinessGalleryItems, _.find(_model.BsinessGalleryItems, { id: itemId }));
                        _model.BsinessGalleryItems.splice(index, 1);
                        deferred.resolve(response);
                    }
                },
                // ERROR
                function(error) {
                    deferred.resolve(error);
                    console.log(error);
                }
            );
            return deferred.promise;
        }

        /**
        * Update or create item on server
        * @param  
        * @returns {*}
        */

        function _createOrUpdateItem(item) {
            var deferred = $q.defer();
            console.log('should happen once');
            var objectTosend = null;
            if(!item.file){
                delete item.file;
            }

            msApi.request('businessGalleryItems@multipartPut', item,
                // SUCCESS
                function(response) {
                    if (response) {
                        var index = _.indexOf(_model.BsinessGalleryItems, _.find(_model.BsinessGalleryItems, { id: response.id }));
                        if (index > -1) {
                            _model.BsinessGalleryItems.splice(index, 1, response);
                            deferred.resolve(response);
                        } else {
                            _model.BsinessGalleryItems.push(response);
                            deferred.resolve(response);
                        }
                    }
                },
                // ERROR
                function(error) {
                    deferred.reject(error);
                    console.log(error);
                }
            );
            return deferred.promise;
        }

        /**
        * Get business gallery items server
        * @param  
        * @returns {*}
        */

        function _getBusinessItems(id) {
            var deferred = $q.defer();
            msApi.request('businessGalleryItems@get', { collectionId: id },
                // SUCCESS
                function(response) {
                    if (response.data) {
                        _model.BsinessGalleryItems = JSON.parse(JSON.stringify(response.data));
                        deferred.resolve(response.data);
                    }
                },
                // ERROR
                function(error) {
                    deferred.resolve(error);
                    console.log(error);
                }
            );
            return deferred.promise;
        }

        /**
        * Get business gallery from server
        * @param  
        * @returns {*}
        */
        function _getBusinessGallery() {
            var deferred = $q.defer();
            msApi.request('businessGallery@get', {},
                // SUCCESS
                function(response) {
                    if (response.data) {
                        _model.loading = false;
                        _model.BusinessGallery = JSON.parse(JSON.stringify(response.data));
                        _model.initialized = true;
                        //Create 'file' property (prepare for send after edit)
                        _.forEach(_model.BusinessGallery, function(value, key) {
                            value.edit = true;
                            //Create 'file' property (prepare for send after edit)
                            if (value.file === undefined || value.file === null) {
                                var img = value.picture.thumb.source;
                                var file = null;
                                blobUtil.imgSrcToBlob(img, 'image/jpeg', null, 1.0).then(function(blob) {
                                    file = blob;
                                    file.lastModifiedDate = new Date();
                                    file.name = value.title;
                                    value.file = file;
                                    return value;
                                }).catch(function(err) {
                                    $log.debug("blobUtil err" + err);
                                });
                            }
                        });
                        deferred.resolve(true);
                    }
                },
                // ERROR
                function(error) {
                    deferred.resolve(error);
                    _model.initialized = false;
                    console.log(error);
                }
            );
            return deferred.promise;
        }
        /**
         * Create business collection folder on server
         * @param folder 
         * @returns {*}
         */
        function _createOrUpdateFolder(folder) {
            var deferred = $q.defer();
            

            console.log("Should happen 1 time");

            var folderObject = {};


            if(folder.id){
                folderObject.id = folder.id;
            }

            if(folder.file){
                folderObject.file = folder.file;
            }

            if(folder.title){
                folderObject.title = folder.title;
            }

            if(folder.index){
                folderObject.index = folder.index;
            }


            _model.BusinessGallery = _.filter(_model.BusinessGallery, function(currentFolder) {
                return currentFolder.id !== undefined;
            });

            // folderObject.file = new File([folderObject.file], folderObject.file.name);

            msApi.request('businessGallery@multipartPut', folderObject,
                // SUCCESS
                function(response) {
                    if (response) {
                        var res = JSON.parse(JSON.stringify(response));
                        var index = _.indexOf(_model.BusinessGallery, _.find(_model.BusinessGallery, { id: response.id }));
                        if (index > -1) {
                            res.edit = true;
                            _model.BusinessGallery.splice(index, 1, res);
                            deferred.resolve(response);
                        } else {
                            res.edit = true;
                            _model.BusinessGallery.push(res);
                            deferred.resolve(response);
                        }
                    }
                },
                // ERROR
                function(error) {
                    deferred.resolve(error);
                    console.log(error);
                }
            );

            return deferred.promise;
        }
        /**
         * Delete business collection folder on server
         * @param folder id
         * @returns {*}
         */
        function _deleteFolder(folderId) {
            var deferred = $q.defer();
            msApi.request('businessGallery@delete', { collectionId: folderId },
                // SUCCESS
                function(response) {
                    if (response) {
                        // var index = _.indexOf(_model.BusinessGallery, _.find(_model.BusinessGallery, { id: folderId }));
                        // _model.BsinessGalleryItems.splice(index, 1);
                        _model.BusinessGallery = _.filter(_model.BusinessGallery, function(currentFolder) {
                            return currentFolder.id !== folderId;
                        });
                        deferred.resolve(true);
                    }
                },
                // ERROR
                function(error) {
                    deferred.resolve(error);
                    console.log(error);
                }
            );

            return deferred.promise;
        }



        // function init() {
        //     _getBusinessGallery();
        // }

        // init();
        /****************** PUBLIC *******************/

        var service = {
            get model() { return _model; },
            set model(m) { _model = m; },
            getBusinessGallery: _getBusinessGallery,
            createOrUpdateFolder: _createOrUpdateFolder,
            deleteFolder: _deleteFolder,
            getBusinessItems: _getBusinessItems,
            createOrUpdateItem: _createOrUpdateItem,
            deleteItem: _deleteItem
        };

        return service;
    }


})();