(function () {
    'use strict';

    angular
        .module('app.gallery', ['ui.sortable'])
        .config(config);


    /** @ngInject */
    function config($stateProvider, $translatePartialLoaderProvider, msApiProvider) {

        $stateProvider
            .state('app.gallery', {
                abstract: true,
                url: '/gallery'
            })
            .state('app.gallery.folders', {
                url: '/folders',
                views: {
                    'content@app': {
                        templateUrl: 'app/main/apps/gallery/views/folders/folders-view.html',
                        controller: 'FoldersController as vm'
                    }
                }
            })
            .state('app.gallery.images', {
                url: '/images',
                views: {
                    'content@app': {
                        templateUrl: 'app/main/apps/gallery/views/images/images-view.html',
                        controller: 'ImagesController as vm'
                    }
                },
                params:{
                    folders:null,
                    itemSelected:null
                }           

            });
     
        // Translation
        $translatePartialLoaderProvider.addPart('app/main/apps/gallery');

    }
})();