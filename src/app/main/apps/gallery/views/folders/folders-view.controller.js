(function() {
    'use strict';

    angular
        .module('app.gallery')
        .controller('FoldersController', FoldersController)
        .controller('addImageDialogController', AddImageDialogController);


    /** @ngInject */
    function FoldersController($mdSidenav, $state, $mdDialog, $document, $log, $scope, PubSub, msApi, msUtils, _, galleryService) {
        var vm = this;
        var formValid = true;

        vm.model = {
            initialized: false,
            showPlus: false
        };

        vm.needFolderSave = false;

        PubSub.subscribe('folderFormValidate', validate);

        function validate(value) {
            formValid = value.valid;
        }

        $log.debug('FoldersController load()');

        /////////////PRIVATE///////////

        /**
         * @name getAllFolders
         * @description init function retrive all folders  
         */
        function getAllFolders() {
            $log.debug('getAllFolders()');
            galleryService.getBusinessGallery().then(function sucsses(res) {
                $log.debug('galleryService.getBusinessGallery() sucsses');
                if (res === true) {
                    vm.folders = galleryService.model.BusinessGallery;
                    vm.model.initialized = true;
                } else {
                    vm.model.initialized = false;
                }
            }, function(error) {
                ///TODO : handle error result
                $log.error('galleryService.getBusinessGallery() error: ' + error);
            });
        }

        /**
         * @name addFolder
         * @description Add folder collection   
         */

        function _addFolder() {
            //Create blob and after that file with default image(prepare sending to server )
            var img = '../assets/images/logos/6.jpg';
            var file = null;
            vm.model.showPlus = true;

            vm.folders.push({});
            var theNewFolder = vm.folders[vm.folders.length - 1];

            $mdDialog.show({
                    controller: AddImageDialogController,
                    templateUrl: 'app/main/apps/gallery/views/addImageDialog/add-image-dialog.html',
                    parent: angular.element($document.find('#main')),
                    targetEvent: theNewFolder,
                    focusOnOpen: true,
                    clickOutsideToClose: true,
                    escapeToClose: true
                })
                .then(function sucsses(img) {
                    var index = vm.folders.length - 1;
                    if (img !== null) {
                        vm.folders[index].file = img[0].lfFile;
                        vm.folders[index].picture = { thumb: { source: img[0].lfDataUrl } };
                        vm.folders[index].edited = true;

                        _editFolder();
                    } else {
                        vm.folders = vm.folders.slice(0, index);
                        vm.model.showPlus = false;
                    }
                    // _changeImageSrc(img);
                    // vm.folders.push({
                    //     businessId: null,
                    //     title: '',
                    //     content: 'אנא ערוך תאור תיקיה',
                    //     picture: { thumb: { source: '../assets/images/logos/6.jpg' } },
                    //     file: file,
                    //     edit: false,
                    //     id: undefined,
                    //     addPictureToFavorite: true
                    // });
                }, function(error) {
                    $log.debug('addFolder() at $mdDialog', error);
                    if (error === null) {
                        var index = vm.folders.length - 1;
                        delete vm.folders[index];
                    }
                    vm.model.showPlus = false;
                });


        }


        function _changeFolderImage(targetFolder) {
            $log.debug(targetFolder);
            targetFolder.edited = true;
            //Create blob and after that file with default image(prepare sending to server )
            var img = '../assets/images/logos/6.jpg';
            var file = null;
            vm.model.showPlus = true;

            $mdDialog.show({
                    controller: AddImageDialogController,
                    templateUrl: 'app/main/apps/gallery/views/addImageDialog/add-image-dialog.html',
                    parent: angular.element($document.find('#main')),
                    targetEvent: targetFolder,
                    focusOnOpen: true,
                    clickOutsideToClose: true,
                    escapeToClose: true
                })
                .then(function sucsses(img) {
                    if (img !== null) {
                        targetFolder.file = img[0].lfFile;
                        _editFolder();
                        vm.model.showPlus = false;

                    }
                }, function(error) {
                    vm.model.showPlus = false;
                    $log.debug('changeFolderImage() at $mdDialog', error);

                });
            vm.model.showPlus = false;

        }
        /**
         * @name moveToImageView
         * @description change state and send the view model to app.gallery.images 
         */

        function _moveToImageView(id) {
            var itemSelected = null;
            if (id) {
                itemSelected = id;
            }
            $state.go('app.gallery.images', { folders: vm.folders, itemSelected: itemSelected });
        }

        /**
         * @name deleteFolder
         * @description delete folder collection 
         */

        function _deleteFolder(ev, folder) {
            //prepare the message format
            var confirm = $mdDialog.confirm()
                .title('האם אתה בטוח שברצונך למחוק?')
                .parent(angular.element(document.querySelector('#foldersView')))
                .targetEvent(ev)
                .clickOutsideToClose(true)
                .ok('מאשר')
                .cancel('ביטול');
            //open dialog
            $mdDialog.show(confirm).then(function() {
                vm.model.initialized = false;
                //for temporary folders(only on the vm)
                if (folder.id === undefined) {
                    vm.folders = _.filter(vm.folders, function(currentFolder) {
                        return currentFolder.id !== undefined;
                    });
                    vm.model.initialized = true;
                    vm.model.showPlus = false;

                } else {
                    //for folders with id(come from server)    
                    galleryService.deleteFolder(folder.id).then(function sucsses(data) {
                        $log.debug('galleryService.deleteFolder() sucsses');
                        if (data) {
                            vm.model.initialized = true;
                            vm.folders = galleryService.model.BusinessGallery;
                            vm.model.showPlus = false;

                        }
                    }, function(error) {
                        ///TODO : handle error result
                        $log.error(' galleryService.deleteFolder() error' + error);
                        vm.model.initialized = true;
                        vm.model.showPlus = false;

                    });
                }

            }, function(error) {
                vm.model.initialized = true;
                vm.model.showPlus = false;
                $log.error('$mdDialog() error' + error);
            });
        }


        /**
         * @name editFolder
         * @description create or updtae folder on server 
         */

        function _editFolder(folder) {
            //Toogle edit icon
            // if (formValid) {
            //     folder.edit = !folder.edit;
            // }
            // //If we on edit mode
            // if (folder.edit && folder.edit === true && formValid) {

            vm.model.initialized = false;
            //Send foler to edit/update
            for (var i = 0; i < vm.folders.length; i++) {
                // console.log(vm.folders[i]);
                if(vm.folders[i].edited === true){
                folder = vm.folders[i];
                vm.needFolderSave = false;
                vm.folders[i].edited = false;
                galleryService.createOrUpdateFolder(folder).then(function sucsses(data) {


                    //request o.k but response with problem
                    if (data.error) {
                        alert(data.error.message + ' :קיימת בעיה בשמירת התמונה');
                        $log.error('galleryService.createOrUpdateFolder() problem: ' + data.error.message);
                        vm.model.initialized = true;
                        vm.model.showPlus = false;
                    } else {
                        //folder update or create successfully
                        vm.model.showPlus = false;
                        vm.model.initialized = true;
                        vm.folders = galleryService.model.BusinessGallery;
                        $log.debug('galleryService.createOrUpdateFolder() sucsses: ');

                    }
                }, function(error) {
                    ///TODO : handle error result
                    alert('קיימת בעיה בשמירת התמונה' + error);
                    $log.error('galleryService.createOrUpdateFolder() error' + error);
                    vm.model.initialized = true;
                    vm.model.showPlus = false;

                });
}
            }

        }


        function _saveNewPosition(s, item) {
            var folderToUpdate = {};
            folderToUpdate.id = item.idToUpdate;
            folderToUpdate.index = item.newIndex;

            galleryService.createOrUpdateFolder(folderToUpdate).then(function sucsses() {
                $log.debug('saveNewPosition sucssess');
            }, function error() {
                $log.error('saveNewPosition faild');
            });

        }



        // $scope.$on('saveNewPosition', _saveNewPosition);

        vm.sortableOptions = {
            update: function(e, ui) {
                var sourceModelArray = ui.item.sortable.sourceModel;
                var newIndex = ui.item.sortable.dropindex;
                var oldIndex = ui.item.sortable.index;
                var updatedItem = ui.item.sortable.model;
                var newRemoteIndex;


                if (oldIndex > newIndex) {
                    newRemoteIndex = sourceModelArray[newIndex].index;
                } else {
                    if (sourceModelArray.length - 1 >= newIndex + 1) {
                        newRemoteIndex = sourceModelArray[newIndex + 1].index;
                    } else {
                        newRemoteIndex = sourceModelArray[sourceModelArray.length - 1].index;
                    }
                }


                var newPosition = {
                    idToUpdate: updatedItem.id,
                    newIndex: newRemoteIndex
                };
                _saveNewPosition(null, newPosition);
                for (var i = newIndex; i < vm.folders.length; i++) {
                    vm.folders[i].index = vm.folders[i].index++;
                }
                updatedItem.index = newRemoteIndex;
            }

        };



        //starter (need to use with $q all)
        getAllFolders();

        ////////////PUBLIC/////////////
        vm.addFolder = _addFolder;
        vm.moveToImageView = _moveToImageView;
        vm.deleteFolder = _deleteFolder;
        vm.editFolder = _editFolder;
        vm.changeFolderImage = _changeFolderImage;



    }

    /**
     * @name AddImageDialog controller
     * @description controller for the dialog 
     */
    /** @ngInject */
    function AddImageDialogController($scope, $mdDialog) {
        var vm = this;
        // $scope.$watch('vm.files', function (newVal, oldVal) {
        //     console.log(vm.files);
        // });
        $scope.hide = function() {
            $mdDialog.hide();

        };
        $scope.cancel = function() {
            $mdDialog.hide(null);


        };
        $scope.answer = function(images) {
            //check the image size

            console.log(images);

            function getMeta(url, callback) {
                var img = new Image();
                img.src = url;
                img.onload = function() { callback(this.width, this.height); };

            }
            getMeta(
                images[0].lfDataUrl,
                function(width, height) {
                    if (width < 250 || height < 250) {
                        alert('קובץ קטן מידי');
                    } else {
                        $mdDialog.hide(images);
                    }
                }
            );
        };


    }

})();
