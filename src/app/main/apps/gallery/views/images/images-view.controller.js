(function() {
    'use strict';
    angular
    .module('app.gallery')
    .controller('ImagesController', ImagesController)
    .controller('SliderController', SliderController);
    var images;
    var itemSelected;
    var direction;
    /** @ngInject */
    function ImagesController($mdSidenav, $state, $scope, $mdDialog, $document, $log, msUtils, msApi, galleryService) {
        var vm = this;
        var selectedFolder = null;
        vm.needImageSave = false;
        vm.saveImages = _saveImages;
        vm.deleteMode = false;
        vm.deleteArr = [];
        //item that user click on folders view
        itemSelected = $state.params.itemSelected;
        //folders for the right side
        vm.folders = $state.params.folders;
        //if user refreshthe page we send him to folder view
        if (!vm.folders) {
            $state.go('app.gallery.folders');
        }

        vm.disablePlusButton = true;
        vm.imagesLoading = false;

        
        vm.imageDelete = _imageDelete;
        vm.addtoDeleteArray = _addtoDeleteArray;



        /////////////PRIVATE///////////

        function _addtoDeleteArray(id){
            var idx = vm.deleteArr.indexOf(id);
            if (idx > -1) {
              vm.deleteArr.splice(idx, 1);
          }
          else {
             vm.deleteArr.push(id);
         }

         //console.log(vm.deleteArr);
         return vm.deleteArr.indexOf(id)> -1;

     }

     function _imageDelete(ev,ans){

        if(ans){
for (var i = 0; i < vm.deleteArr.length; i++) {
    _deleteImage(ev,vm.deleteArr[i]);
}
vm.imageDelete(false);
        }
        else{
            vm.deleteMode = false;
            vm.deleteArr.length = 0;
            vm.checkboxState = false;
        }

    }
    function _addImages(ev) {
        $mdDialog.show({
            controller: DialogController,
            templateUrl: 'app/main/apps/gallery/views/images/addImgDialog/add-image-dialog.html',
            parent: angular.element($document.find('#main')),
            targetEvent: ev,
            clickOutsideToClose: true,
            escapeToClose: true,
            focusOnOpen: true
        })
        .then(function(images) {
            _changeImageSrc(images);
        }, function() {});

    }

    function _moveToSlider(ev) {
        $mdDialog.show({
            controller: SliderController,
            templateUrl: 'app/main/apps/gallery/views/images/sliderDialog/slider-dialog.tmpl.html',
            parent: angular.element($document.find('#main')),
            targetEvent: ev,
            focusOnOpen: true,
            clickOutsideToClose: true,
            escapeToClose: true // Only for -xs, -sm breakpoints.
        }).then(function(img) {
                vm.images = images;
            }, function() {});
        }

        function _backToFoldersState() {
            $state.go('app.gallery.folders');
        }

        function _deleteImage(event, itemId) {
            event.stopImmediatePropagation();
            event.preventDefault();
            vm.imagesLoading = false;
console.log('ay');
            galleryService.deleteItem(itemId).then(function sucsses(data) {
               
                    vm.images = _.filter(vm.images, function(currentimg) {
                        return currentimg.id !== itemId;
                    });
                    images = vm.images;
                    vm.imagesLoading = true;
                
            }, function(error) {
                $log.debug('deleteImage() : ', error);
                vm.imagesLoading = true;
                alert('problem with _deleteImage' + error);

            });
        }


        function _changeImageSrc(images) {
            var items = [];
            vm.imagesLoading = false;

            _.forEach(images, function(img) {
                var item = {};
                item.file = img.lfFile;
                item.title = '';
                //item.title = img.lfFileName;

                item.collectionId = selectedFolder;
                item.content = img.content || '';
                galleryService.createOrUpdateItem(item).then(function sucsses(data) {
                    vm.imagesLoading = true;
                    var image = data;
                    image.edit = true;
                    vm.images.push(image);
                    images = vm.images;
                },
                function(error) {
                    var a = error;
                    alert("changeImageSrc error: " + error);
                });

                items.push(item);
            });

        }


        function _folderSelected(folder) {
            _.forEach(vm.folders, function(currentFolder) {
                //Toggle select button
                if (currentFolder.id === folder.id) {
                    selectedFolder = currentFolder.id;
                    if (currentFolder.show === null || currentFolder.show === undefined) {
                        vm.imagesLoading = false;
                        currentFolder.show = true;
                        //Global var to know if folder selected
                        vm.disablePlusButton = false;
                        getAllImages(currentFolder.id);
                    } else {
                        currentFolder.show = !currentFolder.show;
                        if (currentFolder.show === true) {
                            vm.disablePlusButton = false;
                            vm.imagesLoading = false;
                            getAllImages(folder.id);

                        } else {
                            vm.disablePlusButton = true;
                            vm.images = [];
                            images = vm.images;
                        }

                    }
                } else {
                    currentFolder.show = false;
                }
            });
        }

        function DialogController($scope, $mdDialog) {
            var vm = this;
            // $scope.$watch('vm.files', function (newVal, oldVal) {
            //     console.log(vm.files);
            // });
            $scope.hide = function() {
                $mdDialog.hide();
            };
            $scope.cancel = function() {
                $mdDialog.cancel();
            };
            $scope.answer = function(images) {
                function getMeta(url, callback) {
                    var img = new Image();
                    img.src = url;
                    img.onload = function() { callback(this.width, this.height); };
                }
                getMeta(
                    images[0].lfDataUrl,
                    function(width, height) {
                        if (width < 380 || height < 380) {
                            alert('קובץ קטן מידי');
                        } else {
                            $mdDialog.hide(images);
                        }
                    }

                    );
            };
        }

        function _editPictureName(image) {
            //change the edit value 
            vm.images = _.forEach(vm.images, function(value) {
                if (value.id === image.id) {
                    value.edit = !image.edit;
                }
                return value;
            });
            images = vm.images;
            var imageToSend = {};
            imageToSend.title = image.title;
            imageToSend.collectionId = selectedFolder;
            imageToSend.file = image.file;
            imageToSend.itemId = image.id;
            imageToSend.content = image.content || null;
            if (image.edit === true) {
                vm.imagesLoading = false;

                galleryService.createOrUpdateItem(imageToSend).then(function sucsses(data) {
                    $log.debug("editPictureName createOrUpdateItem()", data);
                    vm.imagesLoading = true;
                }, function(error) {
                    $log.error("editPictureName createOrUpdateItem()", error);
                    vm.imagesLoading = true;
                    alert("editPictureName createOrUpdateItem()" + error);
                })
            }


        }

        function _editFolderTitle(folder) {
            vm.imagesLoading = false;
            //Send foler to edit/update
            galleryService.createOrUpdateFolder(folder).then(function sucsses(data) {
                //request o.k but response with problem
                if (data.error) {
                    alert(data.error.message + " :קיימת בעיה בשמירת התמונה");
                    $log.error("galleryService.createOrUpdateFolder() problem: " + data.error.message);
                    vm.imagesLoading = true;

                } else {
                    //folder update or create successfully
                    $log.debug("galleryService.createOrUpdateFolder() sucsses: ");
                    vm.imagesLoading = true;

                }
            }, function(error) {
                ///TODO : handle error result
                alert("קיימת בעיה בשמירת התיקיה וההודעה הזאת מכוערת למדי יטופל בהמשך");
                $log.error("galleryService.createOrUpdateFolder() error" + error);
                vm.imagesLoading = true;

            });
        }

        function getAllImages(itemSelected) {
            galleryService.getBusinessItems(itemSelected).then(function sucsses(data) {
                vm.images = data;
                vm.images = _.forEach(vm.images, function(img) {
                    img.edit = true;
                    return img;
                });
                images = vm.images;
                vm.imagesLoading = true;
            }, function(error) {
                $log.debug("galleryService.getBusinessItems: " + error);
                alert("galleryService.getBusinessItems:" + error);
            });
        }

        function init() {
            if (vm.folders) {
                getAllImages(itemSelected);
                var index = _.indexOf(vm.folders, _.find(vm.folders, { id: itemSelected }));
                vm.folders[index].show = true;
                //global to know if folder selected
                vm.disablePlusButton = false;
                selectedFolder = vm.folders[index].id;
            }
        }

        function _saveNewPosition(item) {
            var itemToUpdate = {};
            itemToUpdate.itemId = item.idToUpdate;
            itemToUpdate.index = item.newIndex;
            itemToUpdate.collectionId = selectedFolder;
            itemToUpdate.title = item.itemTitle;
            itemToUpdate.content = item.content || null;
            galleryService.createOrUpdateItem(itemToUpdate).then(function sucsses() {
                $log.debug('saveNewPosition sucssess');
            }, function error() {
                $log.error('saveNewPosition failed');
            });

        }


        function _saveImages() {


            //Send foler to edit/update
            for (var i = 0; i < vm.images.length; i++) {
                if(vm.images[i].edited){
vm.images[i].edited = false;
                var imageTemp;
                imageTemp = vm.images[i];
                var imageToSend = {};
                imageToSend.title = imageTemp.title;
                imageToSend.collectionId = selectedFolder;
                imageToSend.file = imageTemp.file;
                imageToSend.itemId = imageTemp.id;
                imageToSend.content = imageTemp.content || null;




                // console.log(vm.images[i]);
                // imagess = vm.images[i];
                // vm.needImageSave = false;
                galleryService.createOrUpdateItem(imageToSend).then(function sucsses(data) {
                    vm.needImageSave = false;

                    //request o.k but response with problem
                    if (data.error) {
                        alert(data.error.message + ' :קיימת בעיה בשמירת התמונה');
                        $log.error(' problem: ' + data.error.message);
                    }
                }, function(error) {
                    ///TODO : handle error result
                    alert(' :קיימת בעיה בשמירת התמונה' + error);
                    $log.error(' error' + error);
                });
            }
        }
        }


        vm.sortableOptions = {
            axis: 'y',

            update: function(e, ui) {
                var sourceModelArray = ui.item.sortable.sourceModel;
                var newIndex = ui.item.sortable.dropindex;
                var oldIndex = ui.item.sortable.index;
                var updatedItem = ui.item.sortable.model;
                var newRemoteIndex;

                if (oldIndex > newIndex) {
                    newRemoteIndex = sourceModelArray[newIndex].index;
                } else {
                    if (sourceModelArray.length - 1 >= newIndex + 1) {
                        newRemoteIndex = sourceModelArray[newIndex + 1].index;
                    } else {
                        newRemoteIndex = sourceModelArray[sourceModelArray.length - 1].index;
                    }
                }


                var newPosition = {
                    idToUpdate: updatedItem.id,
                    itemTitle: updatedItem.title,
                    content: updatedItem.content,
                    newIndex: newRemoteIndex
                };
                _saveNewPosition(newPosition);
                for (var i = newIndex; i < vm.folders.length; i++) {
                    vm.folders[i].index = vm.folders[i].index++;
                }
                updatedItem.index = newRemoteIndex;
            }
        };
        init();
        ////////////PUBLIC/////////////
        vm.addImages = _addImages;
        vm.backToFoldersState = _backToFoldersState;
        vm.deleteImage = _deleteImage;
        vm.folderSelected = _folderSelected;
        vm.moveToSlider = _moveToSlider;
        vm.editPictureName = _editPictureName;
        vm.editFolderTitle = _editFolderTitle;
    }
    /** @ngInject */
    function SliderController($scope, $mdDialog, $log, PubSub, galleryService) {
        var index = 0;
        var vm = this;
        $scope.hide = function() {
            $mdDialog.hide();
        };
        $scope.cancel = function() {
            $mdDialog.cancel();
        };
        $scope.answer = function() {
            $mdDialog.hide();
        };
        images = _.forEach(images, function(value) {
            value.url = value.picture.hd.source;
            value.thumbUrl = value.picture.thumb.source;
            return value;
        });

        $scope.images = images;
        $scope.imageName = images[index].title;

        $scope.$on("onMoveImage", function(evt, data) {
            index = data.index;
            direction = data.name;
            $scope.imageName = images[index].title;
            $log.debug("onMoveImage index: " + data.index + "onMoveImage direction: " + data.name);
        });

        $scope.deleteImage = function _deleteImage() {

            galleryService.deleteItem(images[index].id).then(function sucsses(data) {
                
                $scope.images.splice(index, 1);
                if (direction === 'next' || direction === 'prev') {
                    PubSub.publish('moveToNextImageByDirection', { direction: direction });
                }


            }, function(error) {
                $log.debug("problem with _deleteImage", error);
                alert("problem with _deleteImage", error);
            });
        }
    }



})();
