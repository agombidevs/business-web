(function() {
    'use strict';

    angular
        .module('app.admin')
        .controller('adminController', adminController);


    /** @ngInject */
    function adminController(msApi, $scope, $timeout, $element, $mdDialog, $q) {
        var vm = this;

        // Data

        vm.fieldTypes = [];
        vm.SearchTerm = '';
        vm.isUpdatingFields = false;
        vm.features = [];
        vm.isUpdatingFeatures = false;

        // Agents
        vm.agents = [];
        vm.isAddingNewAgent = false;
        vm.newAgentPassword = false;

        // HotPushing
        vm.isHotpushing = false;
        vm.isSavingDetails = false;

        // Images
        vm.asset = [];
        vm.isSavingAsset = false;
        vm.assetLocalFile = null;
        vm.disableUpload = true;
        vm.icon = [];
        vm.isSavingIcon = false;
        vm.iconLocalFile = null;
        vm.iconDisableUpload = true;

        // Product Components
        vm.product = [];
        vm.selectedActivity = [];


        //business Details

        // Styles
        vm.isCalculatingStyle = false;
        vm.pColor = null;
        vm.aColor = null;
        vm.saveStyleDisable = true;


        vm.colorsDictionary = {
            'Red': '#F44336',
            'Pink': '#E91E63',
            'Purple': '#9C27B0',
            'Deep Purple': '#673AB7',
            'Indigo': '#3F51B5',
            'Blue': '#2196F3',
            'Light Blue': '#03A9F4',
            'Cyan': '#00BCD4',
            'Teal': '#009688',
            'Green': '#4CAF50',
            'Light Green': '#8BC34A',
            'Lime': '#CDDC39',
            'Yellow': '#FFEB3B',
            'Amber': '#FFC107',
            'Orange': '#FF9800',
            'Deep Orange': '#FF5722',
            'Brown': '#795548',
            'Grey': '#9E9E9E',
            'Blue Grey': '#607D8B'
        };
        vm.categoryDictionary = [
            'Book',
            'Business',
            'Apps.Catalogs',
            'Education',
            'Entertainment',
            'Finance',
            'Apps.Food_Drink',
            'Games',
            'Healthcare_Fitness',
            'Lifestyle',
            'Medical',
            'Music',
            'Navigation',
            'News',
            'Apps.Newsstand',
            'Photography',
            'Productivity',
            'Reference',
            'Apps.Shopping',
            'SocialNetworking',
            'Sports',
            'Stickers',
            'Travel',
            'Utilities',
            'Weather'
        ];

        vm.assetOptions = [
            'featureGraphic',
            'coverPhoto1',
            'coverPhoto2',
            'coverPhoto3'
        ];
        var allowFileExtentions = {
            png: 1

        };
        vm.ngFlow = {
            // ng-flow will be injected into here through its directive
            flow: {}
        };

        // Methods
        vm.fileAdded = fileAdded;


        // Methods
        vm.loadProjects = loadProjects;
        vm.saveProject = saveProject;
        vm.calcStyle = calcStyle;
        vm.saveStyle = saveStyle;
        vm.onProjectSelected = onProjectSelected;
        vm.revertStyle = revertStyle;
        vm.loadAssets = loadAssets;
        vm.onAssetSelect = onAssetSelect;
        vm.clearSearchTerm = clearSearchTerm;
        vm.updateField = updateField;
        vm.deleteField = deleteField;
        vm.onFieldSelect = onFieldSelect;
        vm.deleteFeature = deleteFeature;
        vm.featureUpdate = featureUpdate;
        vm.onFeatureSelect = onFeatureSelect;
        vm.executeHotpush = executeHotpush;
        vm.updateProjectAndBusinessDetails = updateProjectAndBusinessDetails;
        vm.updateAsset = updateAsset;
        vm.updateIcon = updateIcon;
        vm.addNewAgent = addNewAgent;
        vm.loadAgents = loadAgents;
        vm.onAgentSelect = onAgentSelect;
        vm.loadAgents = loadAgents;






        function init() {
            //Get all non mandatory fields
            msApi.request('fieldTypes@get', {},
                // SUCCESS
                function(response) {
                    for (var k in response.data) {
                        if (response.data.hasOwnProperty(k)) {
                            vm.fieldTypes.push(response.data[k]);
                        }
                    }
                },
                // ERROR
                function(response) {
                    console.log(response);
                }
            );

            msApi.request('features@get', {},

                // SUCCESS
                function(response) {
                    for (var k in response.data) {
                        if (response.data.hasOwnProperty(k)) {
                            vm.features.push(response.data[k]);

                        }
                    }
                },
                // ERROR
                function(response) {
                    console.log(response);
                }
            );
        }

        init();




        function loadProjects() {

            return msApi.request('projects@get', {},
                // SUCCESS
                function(response) {
                    vm.projects = response.data;

                },
                // ERROR
                function(response) {
                    console.log(response);
                }
            );
        }

        function saveProject() {
            return msApi.resolve('projects@save',

                {
                    bundleId: vm.project.bundleId,
                    project: {
                        appName: vm.project.appName,
                        awaitingUpdates: vm.project.awaitingUpdates,
                        longDesc: vm.project.longDesc,
                        shortDesc: vm.project.shortDesc,
                        keywords: vm.project.keywords,
                        storeTitle: vm.project.storeTitle,
                        categoryIos: vm.project.categoryIos
                    }
                }
                ).then(
                // SUCCESS

                function(response) {

                }).catch(

                // ERROR
                function(response) {
                    console.log(response);
                }

            );
        }

        function onProjectSelected() {
            vm.defaultstyle = vm.project.style;
            loadProduct();
        }


        function loadProduct() {

            return msApi.request('product@get', {
                    bundleId: vm.project.bundleId
                },
                // SUCCESS
                function(response) {
                    vm.product = response.data;
                    // console.log(vm.product);
                },
                // ERROR
                function(response) {
                    console.log(response);
                }
            );
        }



        function loadAssets() {

            return msApi.request('assets@get', {
                    bundleId: vm.project.bundleId,
                    onlyOverrideRequired: true

                },
                // SUCCESS

                function(response) {
                    vm.assets = response.data;
                    //console.log(response);
                },
                // ERROR
                function(response) {
                    console.log(response);
                }
            );
        }

        function onAssetSelect() {
            vm.assetLocalFile = null;
            vm.disableUpload = true;

        }

        function revertStyle() {
            vm.project.style = vm.defaultstyle;
        }


        function calcStyle() {
            vm.isCalculatingStyle = true;

            return msApi.request('calc@get', {
                    primaryColor: vm.pColor,
                    accentColor: vm.aColor

                },
                // SUCCESS

                function(response) {
                    vm.isCalculatingStyle = false;
                    vm.saveStyleDisable = false;
                    vm.project.style = response.data;
                    //console.log(response);
                },
                // ERROR
                function(response) {
                    console.log(response);
                    vm.isCalculatingStyle = false;

                }
            );
        }

        function saveStyle() {
            vm.isCalculatingStyle = true;
            // var styletemp;
            // if (vm.project.style !== vm.defaultstyle) {
            //      styletemp = [vm.project.style];


            // } else {
            //     console.log(vm.project.style);
            //     console.log(vm.defaultstyle);
            //     styletemp = '';
            // }

            console.log(vm.project.style);

            return msApi.request('style@save', {
                    bundleId: vm.project.bundleId,
                    style: vm.project.style
                },
                // SUCCESS

                function(response) {
                    vm.isCalculatingStyle = false;
                    // vm.project.style = response.data;
                    //console.log(response);
                },
                // ERROR
                function(response) {
                    //console.log(response);
                    vm.isCalculatingStyle = false;

                }
            );
        }


        function loadAgents() {

            return msApi.request('agent@get', {
                    businessId: vm.project.business.id,
                },
                // SUCCESS
                function(response) {
                    vm.agents = response.data;
                    console.log(response.data);
                },
                // ERROR
                function(response) {
                    console.log(response);
                }
            );
        }

        function onAgentSelect() {
            //vm.defaultstyle = vm.project.style;
            // console.log(vm.project.businessId);

            //loadProduct();
            //console.log(vm.project);
        }

        function addNewAgent(ev) {
            vm.isAddingNewAgent = true;
            var newAgentDetails = {
                businessId: vm.project.business.id,
                username: vm.selectedAgent.username,
                email: vm.selectedAgent.email,
                firstName: vm.selectedAgent.firstName,
                lastName: vm.selectedAgent.lastName
            };
            if (vm.selectedAgent.id != null) {
                newAgentDetails.agentId = vm.selectedAgent.id;

            }
            if(vm.newAgentPassword){
                newAgentDetails.password = vm.selectedAgent.password;

            }


            msApi.request('agent@save', newAgentDetails,
                // SUCCESS
                function(response) {
                    vm.isAddingNewAgent = false;

                    $mdDialog.show(
                        $mdDialog.alert()
                        .parent(angular.element(document.querySelector('#admin-content')))
                        .clickOutsideToClose(true)
                        .parent(angular.element(document.body))
                        .title('הסוכן' + vm.selectedAgent.username + ' נוצר!')
                        .ok('תודה!')
                        .targetEvent(ev)

                    );
                },
                // ERROR
                function(response) {
                    vm.isAddingNewAgent = false;

                    console.log(response);
                }
            );


        }

        function updateProjectAndBusinessDetails(ev) {
            var deferred = $q.defer();

            vm.isSavingDetails = true;

            msApi.resolve('businessDetails@save', {
                    businessId: vm.project.business.id,
                    businessDetails: vm.project.business.businessDetails
                }).then(
                // SUCCESS
                function(response) {


                    return vm.saveProject();
                }).then(function(response){

                    vm.isSavingDetails = false;

                    if(ev){
                        $mdDialog.show(
                            $mdDialog.alert()
                            .parent(angular.element(document.querySelector('#admin-content')))
                            .clickOutsideToClose(true)
                            .parent(angular.element(document.body))
                            .title('הנתונים נשמרו בהצלחה!')
                            .ok('תודה!')
                            .targetEvent(ev)

                        );
                    }

                    deferred.resolve();

                }).catch(
                // ERROR
                function(response) {
                    console.log(response);

                    deferred.reject();
                }
            );

            return deferred.promise;
        }



        function onFieldSelect() {
            vm.isUpdatingFields = true;
            // Adding the field 
            var fields = [{
                id: vm.selectedField.id
            }];
            msApi.request('fieldTypes@save', {
                    fields: fields,
                    businessId: vm.project.business.id
                },
                // SUCCESS
                function(response) {

                    vm.isUpdatingFields = false;
                    var createdField = response.data[0];
                    //Add to fields list if doesnt exist
                    var businessFields = vm.project.business.fields;
                    for (var i = businessFields.length - 1; i >= 0; i--) {
                        if (businessFields[i].id === createdField.id) {
                            return;
                        }
                    }
                    businessFields.push(createdField);
                },
                // ERROR
                function(response) {
                    console.log(response);
                }
            );
        }

        function deleteField(fieldTypeId) {
            vm.isUpdatingFields = true;

            var fields = [
                fieldTypeId
            ];

            msApi.request('fieldTypes@delete', {
                    fields: JSON.stringify(fields),
                    businessId: vm.project.business.id
                },
                // SUCCESS
                function(response) {
                    vm.isUpdatingFields = false;
                    //Delete the field from the local list
                    var businessFields = vm.project.business.fields;
                    for (var i = businessFields.length - 1; i >= 0; i--) {
                        if (businessFields[i].fieldTypeId === fieldTypeId) {
                            businessFields.splice(i, 1);
                            return;
                        }
                    }
                },
                // ERROR
                function(response) {
                    console.log(response);
                }
            );
        }


        function clearSearchTerm() {
            vm.searchTerm = '';
        }




        function updateField(businessField) {
            vm.isUpdatingFields = true;

            //First delete all 
            var fields = [{
                id: businessField.fieldTypeId,
                index: businessField.index,
                required: businessField.isRequired
            }];


            msApi.request('fieldTypes@save', {
                    fields: fields,
                    businessId: vm.project.business.id
                },
                // SUCCESS
                function(response) {
                    vm.isUpdatingFields = false;
                },
                // ERROR
                function(response) {
                    console.log(response);
                }
            );
        }



        function onFeatureSelect() {
            vm.isUpdatingFeatures = true;

            var feature = vm.selectedFeature;

            // Adding the feature 
            msApi.request('features@save', {
                    bundleId: vm.project.bundleId,
                    featureId: feature.id
                },
                // SUCCESS
                function(response) {
                    vm.isUpdatingFeatures = false;
                    var createdFeature = response.data;
                    //Set the feature instance into the featureSet manually
                    createdFeature.feature = feature;
                    vm.project.featureSet.push(createdFeature);
                },
                // ERROR
                function(response) {
                    console.log(response);
                }
            );
        }


        function featureUpdate(displayName, id) {
            vm.isUpdatingFeatures = true;
            // Adding the feature 
            if (displayName === '') {
                return;
            }
            console.log(vm.project.featureSet);



            msApi.request('features@save', {
                    bundleId: vm.project.bundleId,
                    featureId: id,
                    displayName: displayName
                },
                // SUCCESS
                function(response) {
                    vm.isUpdatingFeatures = false;

                    return;
                },
                // ERROR
                function(response) {
                    console.log(response);
                }
            );
        }


        function deleteFeature(feature) {
            vm.isUpdatingFeatures = true;

            msApi.request('features@delete', {
                    projectId: vm.project.id,
                    featureId: feature.featureId
                },
                // SUCCESS
                function(response) {
                    vm.isUpdatingFeatures = false;
                    //Delete the features from the local list
                    var businessFeatures = vm.project.featureSet;
                    for (var i = businessFeatures.length - 1; i >= 0; i--) {
                        if (businessFeatures[i].id === feature.id) {
                            businessFeatures.splice(i, 1);
                            return;
                        }
                    }
                },
                // ERROR
                function(response) {
                    console.log(response);
                }
            );
        }


        function fileAdded(fileFlow, target) {
            if (allowFileExtentions[fileFlow.getExtension()]) {
                vm.ngFlow.flow.cancel();
                // Prepare the temp file data for media list
                if (target === 'assets') {
                    vm.assetLocalFile = fileFlow;
                    vm.disableUpload = false;
                    return;
                }
                if (target === 'icon') {
                    vm.iconLocalFile = fileFlow;
                    vm.iconDisableUpload = false;

                    return;
                }
            }
        }

        function updateAsset(ev) {
            vm.isSavingAsset = true;

            msApi.request('asset@multipartPost', {
                    bundleId: vm.project.bundleId,
                    assetName: vm.selectedAsset.name,
                    file: vm.assetLocalFile.file,
                    toDelete: false
                },
                // SUCCESS
                function(response) {
                    vm.isSavingAsset = false;
                    //console.log(response);
                    $mdDialog.show(
                        $mdDialog.alert()
                        .parent(angular.element(document.querySelector('#admin-content')))
                        .clickOutsideToClose(true)
                        .parent(angular.element(document.body))
                        .title('תמונה ' + vm.selectedAsset.name + ' הועלתה בהצלחה!')
                        .ok('תודה!')
                        .targetEvent(ev)

                    );
                },
                // ERROR
                function(response) {
                    console.log(response);
                }
            );
        }


        function updateIcon(ev) {
            vm.isSavingIcon = true;

            msApi.request('uploadIcon@multipartPost', {
                    bundleId: vm.project.bundleId,
                    file: vm.iconLocalFile.file
                },
                // SUCCESS
                function(response) {
                    vm.isSavingIcon = false;
                    vm.iconDisableUpload = true;
                    //console.log(response);
                    $mdDialog.show(
                        $mdDialog.alert()
                        .parent(angular.element(document.querySelector('#admin-content')))
                        .clickOutsideToClose(true)
                        .parent(angular.element(document.body))
                        .title('האייקון הועלה בהצלחה!')
                        .ok('תודה!')
                        .targetEvent(ev)

                    );
                },
                // ERROR
                function(response) {
                    console.log(response);
                }
            );
        }




        function executeHotpush(ev) {
            vm.isHotpushing = true;

            updateProjectAndBusinessDetails().then(function(){
                msApi.request('product@save', {
                        bundleId: vm.project.bundleId
                    },
                    // SUCCESS
                    function(response) {
                        vm.isHotpushing = false;

                        $mdDialog.show(
                            $mdDialog.alert()
                            .parent(angular.element(document.querySelector('#admin-content')))
                            .clickOutsideToClose(true)
                            .parent(angular.element(document.body))
                            .title('עדכון מהיר נשלח!')
                            .textContent('.\n גרסה: ' + response.hotpushVersion)
                            .ariaLabel('עדכון מהיר נשלח!')
                            .ok('אוקיי!')
                            .targetEvent(ev)
                        );
                    },
                    // ERROR
                    function(err) {
                        console.log(err);
                    }
                );
            }).catch(function(err){
                console.log(err);
            });

    

        }

        function popMessage(title, content, ev) {
            $mdDialog.show(
                $mdDialog.alert()
                .parent(angular.element(document.querySelector('#admin-content')))
                .clickOutsideToClose(true)
                .parent(angular.element(document.body))
                .title(title)
                .textContent(content)
                .ariaLabel(title)
                .ok('אוקיי!')
                .targetEvent(ev)

            );
            console.log(title + ' ' + content);

        }


        //////////

    }
})();
