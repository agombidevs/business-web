(function() {
    'use strict';

    angular
        .module('app.admin', [
            'mdColorPicker'
        ])
        .config(config);

    /** @ngInject */
    function config($stateProvider, $translatePartialLoaderProvider, msApiProvider, msNavigationServiceProvider) {
        // State
        $stateProvider
            .state('app.admin', {
                url: '/admin',
                views: {
                    'content@app': {
                        templateUrl: 'app/main/apps/admin/admin.html',
                        controller: 'adminController as vm'
                    }
                },
                data: {
                    permissions: {
                        only: 'ADMIN',
                        redirectTo: 'app.pages_auth_login-v2'
                    }
                }
            });

        // Translation
        $translatePartialLoaderProvider.addPart('app/main/apps/admin');


        // Api
        msApiProvider.register('projects', ['serviceInterface/projects', {
            filter: {
                include: [{
                        business: [
                            'businessDetails',
                            'fields'
                        ],
                    },
                    'style', {
                        featureSet: 'feature'
                    }
                ]
            }
        }]);


        msApiProvider.register('fieldTypes', ['serviceInterface/business/fields']);
        msApiProvider.register('businessDetails', ['serviceInterface/business/details']);
        msApiProvider.register('product', ['serviceInterface/project/product']);

        //GET POST n DELETE Features
        msApiProvider.register('features', ['serviceInterface/business/features']);

        //POST Asset
        msApiProvider.register('asset', ['serviceInterface/project/asset']);

        //GET Assets
        msApiProvider.register('assets', ['serviceInterface/project/assets']);

        //GET New style pallet
        msApiProvider.register('calc', ['serviceInterface/project/style/calc']);
        msApiProvider.register('uploadIcon', ['serviceInterface/project/uploadIcon']);

        //GET Post New Agent
        msApiProvider.register('agent', ['serviceInterface/business/agent']);

        //POST New Style
        msApiProvider.register('style', ['serviceInterface/project/style']);

        //GET PRODUCT
         msApiProvider.register('product', ['serviceInterface/project/product']);

    }



})();
