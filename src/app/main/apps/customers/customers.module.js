(function ()
{
    'use strict';

    angular
        .module('app.customers',
            [
                // 3rd Party Dependencies
                'xeditable',
                'datatables',
                'flow'
            ]
        )
        .config(config)
        .run(runBlock);

    /** @ngInject */
    function config($stateProvider, $translatePartialLoaderProvider, msApiProvider)
    {

        $stateProvider.state('app.customers', {
            url    : '/customers',
            views  : {
                'content@app': {
                    templateUrl: 'app/main/apps/customers/customers.html',
                    controller : 'ContactsController as vm'
                }
            },
            data: {
                permissions: {
                    only: 'AUTHORIZED',
                    redirectTo: 'app.pages_auth_login-v2'
                }
            }
        });


        msApiProvider.register('customers', ['businessCustomers']);
        msApiProvider.register('fields', ['businessFields']);

        msApiProvider.register('announcements', ['businessAnnouncements']);



        // Translation
        $translatePartialLoaderProvider.addPart('app/main/apps/customers');


    }


    /** @ngInject */
    function runBlock(DTDefaultOptions)
    {
        DTDefaultOptions.setDOM('<"top"f>rt<"bottom"<"left"<"info"i><"pagination"p>><"right"<"length"l>>>');


    }

})();