(function ()
{
    'use strict';

    angular
        .module('app.customers')
        .controller('CustomerDialogController', CustomerDialogController);

    /** @ngInject */
    function CustomerDialogController($mdDialog, Customer, msUtils)
    {
        var vm = this;

        // Data
        vm.title = 'Edit Contact';
        vm.customer = angular.copy(Customer);
        vm.newContact = false;
        vm.allFields = false;

        // Methods
        vm.closeDialog = closeDialog;
        vm.toggleInArray = msUtils.toggleInArray;
        vm.exists = msUtils.exists;



        /**
         * Close dialog
         */
        function closeDialog()
        {
            $mdDialog.hide();
        }

    }
})();