(function() {
    'use strict';

    angular
        .module('app.customers')
        .controller('NewAnnouncementDialogController', NewAnnouncementDialogController);

    /** @ngInject */
    function NewAnnouncementDialogController($log, $mdDialog, $timeout, msApi, CustomerIds, visibilityType, showFutureUsersCheckbox, customerFilter, $scope) {
        var vm = this;

        // Data        
        vm.title = 'שליחת עדכון';
        vm.allFields = false;
        vm.futureUsers = true;
        vm.visibilityType = visibilityType;
        vm.showFutureUsersCheckbox = showFutureUsersCheckbox;
        vm.announcement = {
            customerFilter: customerFilter,
            customerIds: CustomerIds
        };

        // Upload Reqs
        vm.fileDimensionsOk = true;
        vm.fileSizeOk = true;
        vm.fileTypeOk = true;
        vm.fileIntSize = 0;
        vm.isProccessing = false;
        vm.fileDWidth = 0;
        vm.fileDHeight = 0;

        vm.showNewAnnouncementForm = true;
        vm.isInProgress = false;
        vm.showSuccessMessage = false;

        vm.ngFlowOptions = {
            // You can configure the ngFlow from here
            /*target                   : 'api/media/image',
             chunkSize                : 10 * 1024 * 1024,
             maxChunkRetries          : 1,
             simultaneousUploads      : 1,
             testChunks               : false,
             progressCallbacksInterval: 1000*/
            chunkSize: 10 * 1024 * 1024,
            singleFile: true
        };
        vm.ngFlow = {
            // ng-flow will be injected into here through its directive
            flow: {}
        };

        // Methods
        vm.fileAdded = fileAdded;
        vm.sendAnnouncement = sendAnnouncement;
        vm.retry = retry;
        vm.closeDialog = closeDialog;


        var allowFileExtentions = {
            png: 1,
            jpg: 1,
            jpeg: 1
        };

        /**
         * File added callback
         * Triggers when files added to the uploader
         *
         * @param file
         */


        function fileAdded(fileFlow) {
            vm.isProccessing = true;
            vm.fileDimensionsOk = true;
            vm.fileSizeOk = true;
            vm.fileTypeOk = true;
            vm.imageFile = null;

            if (!allowFileExtentions[fileFlow.getExtension()]) {
                vm.fileTypeOk = false;
                vm.fileType = fileFlow.getExtension();
                vm.isProccessing = false;
                return;
            }


            if (fileFlow.size >= vm.ngFlowOptions.chunkSize) {
                var fileIntSize = (fileFlow.size / 1000 / 1000).toFixed(2);
                vm.fileIntSize = fileIntSize;
                vm.fileSizeOk = false;
            }



            var fileReader = new FileReader();
            fileReader.readAsDataURL(fileFlow.file);
            fileReader.onload = function(event) {
                var img = new Image();
                img.onload = function() {
                    fileFlow.file.dimensions = {
                        width: this.width,
                        height: this.height
                    };

                    fileFlow.dimensions = fileFlow.file.dimensions;
                    if (fileFlow.dimensions.width < 380 || fileFlow.dimensions.height < 200) { //dimensions validator
                        vm.fileDWidth = fileFlow.dimensions.width;
                        vm.fileDHeight = fileFlow.dimensions.height;
                        vm.fileDimensionsOk = false;
                    }


                    if (vm.fileTypeOk && vm.fileSizeOk && vm.fileDimensionsOk) {

                        // Prepare the temp file data for media list
                        vm.imageFile = fileFlow;
                    }

                    vm.isProccessing = false;

                    _.defer(function() {
                        $scope.$apply();
                    });
                };

                img.src = event.target.result;
            };
        }



        function sendAnnouncement(ev) {
            vm.isInProgress = true;
            vm.showNewAnnouncementForm = false;

            // Set the file to announcement
            vm.announcement.file = vm.imageFile.file;

            if(!vm.futureUsers){
                vm.announcement.visibilityType = 'specificUsers';
            } else {
                vm.announcement.visibilityType = vm.visibilityType;
            }

            msApi.request('announcements@multipartPost', vm.announcement,

                // SUCCESS
                function(response) {
                    vm.showSuccessMessage = true;
                    vm.isInProgress = false;
                    //Close the dialog after 2 Seconds
                    $timeout(vm.closeDialog, 2000);
                },
                // ERROR
                function(response) {
                    $timeout(function(){
                        vm.isInProgress = false;
                        vm.showErrorMessage = true;
                    },700);

                    $log.log(response);
                }
            );
        }

        function retry(){
            vm.showErrorMessage = false;
            vm.showNewAnnouncementForm = true;
        }



        /**
         * Close dialog
         */
        function closeDialog() {
            $mdDialog.hide();
        }

    }
})();
