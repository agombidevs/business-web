(function ()
{
    'use strict';

    angular
        .module('app.customers')
        .controller('ContactsController', ContactsController);

    /** @ngInject */
    function ContactsController($mdSidenav, msApi, msUtils, $mdDialog, $document, $log, DTOptionsBuilder, $scope, DTColumnDefBuilder, $q)
    {
        var vm = this;


        
        // Data
        vm.whereFilter = {};
        vm.filterIds = null;
        vm.listType = 'all';
        vm.listOrder = 'name';
        vm.listOrderAsc = false;
        vm.newGroupName = '';
        vm.selected = {};
        vm.selectAll = false;
        vm.isLoading = true;

        // Methods
        vm.openCustomerDialog = openCustomerDialog;
        vm.openNewAnnouncementDialog = openNewAnnouncementDialog;
        vm.filterCustomers = filterCustomers;
        vm.clearFilters = clearFilters;
        vm.isFiltered = isFiltered;
        vm.toggleContainValueFilter = toggleContainValueFilter;
        vm.ageFieldChanged = ageFieldChanged;
        vm.isValueInqFiltered = isValueInqFiltered;
        vm.toggleSidenav = toggleSidenav;
        vm.exists = msUtils.exists;
        vm.toggleAll = toggleAll;
        vm.toggleOne = toggleOne;
        vm.selectedIdsArray = selectedIdsArray;

        //////////


        initialController();



        function initialController(){
            $q.all([
                msApi.resolve('customers@get'),
                msApi.resolve('fields@get')
            ]).then(function(results){
                vm.customers = results[0].customers;
                vm.fields = results[1].fields;

                //Fields configure

                //Refine fields
                refineFields();


                //Add extra dynamic field as age
                refineCustomers();


                //Init the dataTable
                initDataTable();

                vm.isLoading = false;
            });
        }



        function initDataTable(){
            
            //Build the datatable options
            vm.dtOptions = DTOptionsBuilder.newOptions()
            .withOption('responsive', true)
            .withPaginationType('simple')
            .withOption('autoWidth', false)
            .withLanguage({
                'sEmptyTable': 'לא נמצאו משתמשים',
                'sInfo': 'מציג  _END_-_START_  מתוך _TOTAL_ משתמשים',
                'sInfoEmpty': 'לא נמצאו משתמשים',
                'sInfoFiltered': '(סונן מתוך _MAX_ סך המשתמשים)',
                'sInfoPostFix': '',
                'sInfoThousands': ',',
                'sLengthMenu': 'הצג _MENU_ משתמשים',
                'sLoadingRecords': 'טוען...',
                'sProcessing': 'Processing...',
                'sSearch': 'חפש:',
                'sZeroRecords': 'No matching records found',
                'oPaginate': {
                    'sFirst': 'ראשון',
                    'sLast': 'אחרון',
                    'sNext': 'הבא',
                    'sPrevious': 'הקודם'
                },
                'oAria': {
                    'sSortAscending': ': activate to sort column ascending',
                    'sSortDescending': ': activate to sort column descending'
                }
            });

            //Build the columns of the table
            buildDataTableColumns();
        }

        function buildDataTableColumns(){
            vm.dtColumnDefs = [
                DTColumnDefBuilder.newColumnDef(0).withOption('width','1px'),
                DTColumnDefBuilder.newColumnDef(1).withOption('sWidth','20px'),
                DTColumnDefBuilder.newColumnDef(2).withOption('sWidth','60px')
            ];

            //The 3 first fields are static so this is our initial index
            var fieldsIndex = 2;

            vm.fields.forEach(function(field){
                //Increament the index every field
                fieldsIndex++;
                if(field.fieldType.inputType === 'date'){
                    vm.dtColumnDefs.push(DTColumnDefBuilder.newColumnDef(fieldsIndex).notSortable());
                }
            });

            //Make last field created date with hidden sortble field (unix date)
            vm.dtColumnDefs.push(DTColumnDefBuilder.newColumnDef(fieldsIndex+1).withOption('orderData', fieldsIndex+2));
            vm.dtColumnDefs.push(DTColumnDefBuilder.newColumnDef(fieldsIndex+2).notVisible());
        }



        function refineFields(){
            var refinedFields = [];

            vm.fields.forEach(function(field){
                switch(field.fieldType.key) {
                    case 'dateOfBirth':
                        var ageField = {
                            fieldType: {
                                name: 'גיל',
                                inputType: 'number',
                                key: 'age'
                            }
                        };
                        refinedFields.push(ageField);
                        break;
                }
                refinedFields.push(field);
            });


            //Override the original fields
            vm.fields = refinedFields;
        }


        function refineCustomers(){
            //Execute field factory on the list
            vm.customers.forEach(function(customer){
                var fields = customer.fields;
                if(fields.dateOfBirth){
                    var dateOfBirth = new Date(fields.dateOfBirth);
                    var ageDifMs = Date.now() - dateOfBirth.getTime();
                    var ageDate = new Date(ageDifMs); // miliseconds from epoch
                    fields.age = Math.abs(ageDate.getUTCFullYear() - 1970);
                } else {
                    fields.age = '';
                }
                if(customer.created){
                    customer.unixCreated = Date.parse(customer.created);
                }
                customer.fields = fields;
            });
        }



        function isEmpty(obj) {
            for(var prop in obj) {
                if(obj.hasOwnProperty(prop)){
                    return false;
                }
            }

            return true && JSON.stringify(obj) === JSON.stringify({});
        }

        function cleanNullFromObject(obj) {

            for (var key in obj) {
                if( obj.hasOwnProperty( key ) ) {
                    var value = obj[key];

                    if (typeof value === 'object') {
                        if(isEmpty(value)){
                            delete obj[key];
                        } else {
                            cleanNullFromObject(value);     
                        }
                    }

                    if (value == null || value === undefined || (Array.isArray(value) && value.length === 0)) {
                        delete obj[key];
                    }
                }
            }

        }


        function ageToDate(age){
            var date = new Date();
            date.setFullYear(date.getFullYear()-age);
            return date;
        }


        function refinedWhereFilter(){
            var whereFilter = angular.copy(vm.whereFilter);

            delete whereFilter['fields.age'];

            for (var fieldKey in whereFilter) {
                if( whereFilter.hasOwnProperty( fieldKey ) ) {
                    var fieldFilter = whereFilter[fieldKey];
                    if(fieldFilter.gte && fieldFilter.lte){
                        //Merge the gte and lte to be in AND logic

                        if(!whereFilter.and){
                            whereFilter.and = [];
                        }
                        var gteFilter = {};
                        gteFilter[fieldKey] = { 
                            gte : fieldFilter.gte 
                        };
                        whereFilter.and.push(gteFilter);
                        // Delete the old filter method
                        delete fieldFilter.gte;

                        var lteFilter = {};
                        lteFilter[fieldKey] = { 
                            lte : fieldFilter.lte 
                        };
                        whereFilter.and.push(lteFilter);
                        // Delete the old filter method
                        delete fieldFilter.lte;
                    }
                }
            }

            //Clean nulls in filter (We do it twice because we dont want empty object also)
            cleanNullFromObject(whereFilter);
            cleanNullFromObject(whereFilter);

            return whereFilter;
        } 


        function ageFieldChanged(value, isGte){
            if(!vm.whereFilter['fields.dateOfBirth']){
                vm.whereFilter['fields.dateOfBirth'] = {};
            }
            if(isGte){
                if(!value){
                    delete vm.whereFilter['fields.dateOfBirth'].lte;
                } else {
                    vm.whereFilter['fields.dateOfBirth'].lte = ageToDate(value); 
                }
            } else {
                if(!value){
                    delete vm.whereFilter['fields.dateOfBirth'].gte;
                } else {
                    vm.whereFilter['fields.dateOfBirth'].gte = ageToDate(value);
                }
            }
        }


        function filterCustomers () {
            vm.isFiltering = true;
            var where = refinedWhereFilter();
            

            msApi.request('customers@get',{ where : where },
                    // SUCCESS
                    function (response)
                    {
                        vm.isFiltering = false;
                        vm.customers = response.customers;
                        toggleSidenav('sidenav');
                        refineCustomers();
                        updateSelectAll();
                    },
                    // ERROR
                    function (response)
                    {
                        $log.log(response);
                    }
            );
        }

        function clearFilters(){
            vm.whereFilter = {};
            vm.filterCustomers();
            updateSelectAll();
        }

        function isFiltered(){
            return (Object.keys(vm.whereFilter).length > 0);
        }


        function toggleContainValueFilter(key, value){
            if(isValueInqFiltered(key,value)){
                var indexOfValue = vm.whereFilter[key].inq.indexOf(value);
                vm.whereFilter[key].inq.splice(indexOfValue, 1);
            } else {
                if(!vm.whereFilter[key] || !vm.whereFilter[key].inq){
                    vm.whereFilter[key] = { inq : [] };
                }
                vm.whereFilter[key].inq.push(value);
            }
        }


        function isValueInqFiltered(key, value){
            if(!vm.whereFilter[key] || !vm.whereFilter[key].inq){
                return false;
            } else if(vm.whereFilter[key].inq.indexOf(value) === -1){
                return false;
            } else {
                return true;
            }
        }


        function toggleAll () {
            vm.selectAll = !vm.selectAll;
            if(vm.selectAll){
                vm.customers.forEach(function(customer){
                    vm.selected[customer.id] = true;
                });
            } else {
                vm.selected = {};
            }
        }

        function toggleOne (customerId) {
            var isSelected = vm.selected[customerId];
            if(isSelected){
                delete vm.selected[customerId];
            } else {
                vm.selected[customerId] = true;
            }
            updateSelectAll();
        }

        function updateSelectAll() {
            var selectedCount = Object.keys(vm.selected).length;
            if(selectedCount === vm.customers.length){
                vm.selectAll = true;
            } else {
                vm.selectAll = false;
            }  
        }

        function selectedIdsArray () {
            return Object.keys(vm.selected);
        }


        /**
         * Open customer dialog
         *
         * @param ev
         * @param customer
         */
        function openCustomerDialog(ev, customer)
        {
            $mdDialog.show({
                controller         : 'CustomerDialogController',
                controllerAs       : 'vm',
                templateUrl        : 'app/main/apps/customers/dialogs/customer/customer-dialog.html',
                parent             : angular.element($document.find('#main')),
                targetEvent        : ev,
                clickOutsideToClose: true,
                locals             : {
                    Customer: customer
                }
            });
        }



        /**
         * Open customer dialog
         *
         * @param ev
         * @param customer
         */
        function openNewAnnouncementDialog(ev)
        {
            //Prepare visibilityType 
            //By the user behavor
            var visibilityType = vm.selectAll ? ( isFiltered() ? 'filtered' : 'anyone' ) : 'specificUsers';

            $mdDialog.show({
                controller         : 'NewAnnouncementDialogController',
                controllerAs       : 'vm',
                templateUrl        : 'app/main/apps/customers/dialogs/new-announcement/new-announcement-dialog.html',
                parent             : angular.element($document.find('#main')),
                targetEvent        : ev,
                clickOutsideToClose: true,
                locals             : {
                    visibilityType: visibilityType,
                    showFutureUsersCheckbox: ( visibilityType !== 'specificUsers'),
                    customerFilter: refinedWhereFilter(),
                    CustomerIds: vm.selectedIdsArray()
                }
            });
        }

    

        /**
         * Toggle sidenav
         *
         * @param sidenavId
         */
        function toggleSidenav(sidenavId)
        {
            $mdSidenav(sidenavId).toggle();
        }

    }

})();