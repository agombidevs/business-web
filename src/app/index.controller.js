(function ()
{
    'use strict';

    angular
        .module('fuse')
        .controller('IndexController', IndexController);

    /** @ngInject */
    function IndexController(fuseTheming, $cookies, navigationService)
    {
        var vm = this;

        // Set the themes
        vm.themes = fuseTheming.themes;


        //Setup navigation items
        navigationService.setupNavigationItems();
        
        //////////
    }
})();