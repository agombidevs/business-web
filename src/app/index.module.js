(function () {
    'use strict';

    /**
     * Main module of the Fuse
     */
    angular
        .module('fuse', [

            // Core
            'app.core',

            // Navigation
            'app.navigation',

            // Toolbar
            'app.toolbar',

            // Quick Panel
            'app.quick-panel',


            //Apps
            'app.dashboards.analytics',
            'app.chat',
            'app.customers',
            'app.gallery',
            'app.admin',
            'app.announcementEditor',


            'app.pages',

            //Services
            'services.auth',
            'services.errorTrack',
            'services.notification',
            'services.style',
            'services.navigation',
            'services.utils',

            'environment',
            
            //3d party
            'PubSub',
            'angularMoment'
            

        ]);
})();