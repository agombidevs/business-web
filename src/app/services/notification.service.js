(function() {
    'use strict';

    angular
        .module('services.notification', [

        ])
        .config(config)
        .factory('notificationService', notificationService)
        .run(runBlock);

    /** @ngInject */
    function config(msApiProvider) {
        msApiProvider.register('installation', ['businessUsers/installation']);
    }


    /** @ngInject */
    function notificationService(msApi, authService, $state) {

        var service = {};




        if (window.cordova) {

	        if(authService.isAuthorized()){
	           initNotifications(); 
	        }

        	authService.addAuthorizedHandler(initNotifications);
        }



        function initNotifications(){

            //Initial code the run only in mobile devices
            window.plugins.OneSignal.setLogLevel({ logLevel: 4 });

            //Init the onesignal service
            window.plugins.OneSignal
                .startInit('5b795622-0abe-4c97-ad1b-7441c3ba4df0', '831694623562')
                .handleNotificationOpened(notificationOpenedCallback)
                .handleNotificationReceived(notificationReceivedCallback)
                .inFocusDisplaying(window.plugins.OneSignal.OSInFocusDisplayOption.None)
                .endInit();


            //Handle oneSignal id availbe
            window.plugins.OneSignal.getIds(oneSignalIdsAvailable);

            document.addEventListener('resume', clearAllNotification, false);

        }



        function clearAllNotification(){
            window.cordova.exec(function(){}, function(){}, 'OneSignalPush', 'clearOneSignalNotifications', []);
        }


        function notificationOpenedCallback(jsonData) {
            console.log('notificationOpenedCallback: ' + JSON.stringify(jsonData));

            //Open the chat
            $state.go('app.chat');
        }

        function notificationReceivedCallback(jsonData) { 
     	    console.log('Did I receive a notification: ' + JSON.stringify(jsonData));
        }


        function oneSignalIdsAvailable(ids) {

            //Prepare installation 
            var installation = {
                deviceId: ids.userId,
                oneSignalPlayerId: ids.userId,
                notificationToken: ids.pushToken
            };

            //Update installation in API
            msApi.request('installation@save', {
                    deviceId: installation.deviceId,
                    data: installation
                },
                // SUCCESS
                function(response) {
                    console.log('Installation updated');
                },
                // ERROR
                function(response) {
                    console.log(response);
                }
            );
        }

        return service;
    }



    //Run the service by default

    /** @ngInject */
    function runBlock(notificationService) {

    }



})();
