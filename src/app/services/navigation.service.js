(function() {
    'use strict';

    angular
        .module('services.navigation', [])
        .config(config)
        .factory('navigationService', navigationService);


    /** @ngInject */
    function config() {
        if (window.cordova) {
            document.addEventListener('backbutton', onBackKeyDown, false);
        }

        function onBackKeyDown() {
            navigator.Backbutton.goHome(function() {
                console.log('success');
            }, function() {
                console.log('fail');
            });
        }
    }

    /** @ngInject */
    function navigationService(msNavigationService, authService, utilsService) {



        var service = {
            setupNavigationItems: _setupNavigationItems
        };

        function _setupNavigationItems() {

            //Gallery
            msNavigationService.saveItem('Gallery', {
                title: 'גלריה',
                icon: 'icon-picture',
                weight: 10,
                state: 'app.gallery.folders',
                hidden: function() {
                    return !authService.isAuthorized();
                }
            });


            //Announcement Editor
            msNavigationService.saveItem('announcementEditor', {
                title: 'עדכונים',
                icon: 'icon-table-edit',
                state: 'app.announcementEditor',
                weight: 10,
                hidden: function() {
                    return !authService.isAuthorized(); 
                }
            });

            //Customers
            msNavigationService.saveItem('Customers', {
                title: 'תצוגת משתמשים',
                icon: 'icon-account-box',
                state: 'app.customers',
                weight: 10,
                hidden: function() {
                    return !authService.isAuthorized();
                }
            });

            //Chat
            msNavigationService.saveItem('Chat', {
                title: 'צ\'אט',
                icon: 'icon-hangouts',
                state: 'app.chat',
                weight: 1,
                hidden: function() {
                    return !authService.isAuthorized();
                }
            });


            //Analytics dashboard
            msNavigationService.saveItem('dashboard-analytics', {
                title: 'נתוני שימוש',
                icon: 'icon-chart-line',
                state: 'app.dashboards_analytics',
                weight: 2,
                badge:{
                    content: 'חדש!',
                    color: '#396FC1'
                },
                hidden: function() {
                    return !authService.isAuthorized();
                }
            });

            

            //Admin
            msNavigationService.saveItem('admin', {
                title: 'ניהול עסקים',
                icon: 'icon-tile-four',
                state: 'app.admin',
                weight: 1,
                hidden: function() {
                    return !authService.isAdmin();
                }
            });
        }


        return service;
    }



})();
