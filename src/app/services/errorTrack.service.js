(function ()
{
    'use strict';

    angular
        .module('services.errorTrack', [])
        .factory('errorTrackService', errorTrackService);



    /** @ngInject */
    function errorTrackService(envService, authService){

        var service = {
            notify: notify
        };


        //Get environment name
        var environment = envService.get();

        var usingErrorTracking = environment === 'production';


        if(usingErrorTracking){
            Raven
                .config('https://62c5277dfbca4a2391c293cb956b0de3@sentry.io/116497', {
                    environment: environment,
                    autoBreadcrumbs: {
                        'xhr': false,      // XMLHttpRequest
                        'console': false,  // console logging
                        'dom': true,       // DOM interactions, i.e. clicks/typing
                        'location': false  // url changes, including pushState/popState
                    },
                    ignoreErrors: [
                        'Websocket closed unexcectedly'
                    ]
                })
                .addPlugin(Raven.Plugins.Angular)
                .install();

            //Get the current access token
            var accessToken = authService.getAccessToken();

            Raven.setExtraContext({
                accessToken: accessToken
            }); 
        }




        function notify(error){
            if(usingErrorTracking){
                if(typeof error === 'object'){
                    error = JSON.stringify(error);
                }
                error = new Error(error);
                Raven.captureException(error);  
            } else {
                console.error(error);
            }
        }

        return service;
    }



})();