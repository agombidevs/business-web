(function() {
    'use strict';

    angular
        .module('services.auth', [
            'permission',
            'permission.ui',
            'ngStorage'
        ])
        .provider('authService', authServiceProvider)
        .run(runBlock);



    /** @ngInject */
    function authServiceProvider(msApiProvider, $localStorageProvider, $sessionStorageProvider){

        $localStorageProvider.setKeyPrefix('ls.');
        $sessionStorageProvider.setKeyPrefix('ls.');

        var provider = this;

        //Constants Keys
        var kAccessToken = 'accessToken';
        var kDevAccessToken = 'devAccessToken';

        var isSessionStorage = false;

        msApiProvider.register('businessesOwnDetails', ['businessUsers/me']);


        this.$get = function ($state, PermRoleStore, PubSub, msNavigationService, $sessionStorage, $localStorage){

            var authService = {
                getAccessToken: getAccessToken,
                setAccessToken: setAccessToken,
                getDevAccessToken: getDevAccessToken,
                setDevAccessToken: setDevAccessToken,
                addAuthorizedHandler: addAuthorizedHandler,
                addUnAuthorizedHandler: addUnAuthorizedHandler,
                clearSession: clearSession,
                isAuthorized: isAuthorized,
                isAdmin: isAdmin
            };



            //Define Permisions

            PermRoleStore.defineRole('AUTHORIZED', function() {
                return isAuthorized();
            });


            PermRoleStore.defineRole('ADMIN', function() {
                return isAdmin();
            });


            function isAuthorized() {
                var accessToken = getAccessToken();
                if (accessToken) {
                    return true;
                } else {
                    return false;
                }
            }



            function isAdmin() {
                var devAccessToken = getDevAccessToken();
                if (devAccessToken) {
                    return true;
                } else {
                    return false;
                }
            }


            function addAuthorizedHandler(callback) {
                PubSub.subscribe('authrorized', callback);
            }


            function addUnAuthorizedHandler(callback) {
                PubSub.subscribe('unauthrorized', callback);
            }


            function getAccessToken(){
                var accessToken = $localStorageProvider.get(kAccessToken);

                if(!accessToken){ 
                    accessToken = $sessionStorageProvider.get(kAccessToken);
                    //If there is no accessToken on sessionStorage it will set the storageType back to localStorage
                    if(accessToken){
                        isSessionStorage = true;        
                    }
                }
                return accessToken; 
            }

            function setAccessToken(accessToken, remember){
                if(!remember){
                    $localStorage.$reset();
                    isSessionStorage = true;
                    $sessionStorageProvider.set(kAccessToken,accessToken);
                } else {
                    isSessionStorage = false;
                    $localStorageProvider.set(kAccessToken,accessToken);
                }
                
                msApiProvider.setAccessToken(accessToken);
                PubSub.publish('authrorized');
            }


            function getDevAccessToken(){
                var devAccessToken = $sessionStorageProvider.get(kDevAccessToken);

                return devAccessToken; 
            }


            function setDevAccessToken(devAccessToken){
                $sessionStorage.$reset();
                $localStorage.$reset();

                $sessionStorageProvider.set(kDevAccessToken,devAccessToken);
                isSessionStorage = true;
                msApiProvider.setDevAccessToken(devAccessToken);
            }


            function clearSession(){
                $sessionStorage.$reset();
                $localStorage.$reset();

                PubSub.publish('unauthrorized');

                $state.go('app.pages_auth_login-v2');
            }

            return authService;
        };

        return provider;
    }


        /** @ngInject */
    function runBlock(msApi,authService)
    {
        
        var isAuthorized = authService.isAuthorized();
        if(isAuthorized){

            //Test the authentication by sending api request the businessUsers/me
            msApi.request('businessesOwnDetails@get', {},
                // SUCCESS
                function (response) {
                },
                // ERROR
                function (error) {
                    if(error.status === 401){
                        authService.clearSession();
                        console.log('disconnected!');   
                    }
                }
            );
        }
    }



})();
