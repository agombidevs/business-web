(function ()
{
    'use strict';

    angular
        .module('services.style', [])
        .factory('styleService', styleService);



    /** @ngInject */
    function styleService($cookies, fuseTheming, $window){


        var service = {
            setStyle: _setStyle,
            resetStyle: _resetStyle
        };

        var primaryColorsDictionary = {
            '#F44336': 'red',
            '#E91E63': 'pink',
            '#9C27B0': 'purple',
            '#673AB7': 'deep-purple',
            '#3F51B5': 'indigo',
            '#2196F3': 'blue',
            '#03A9F4': 'light-blue',
            '#00BCD4': 'cyan',
            '#009688': 'teal',
            '#4CAF50': 'green',
            '#8BC34A': 'light-green',
            '#CDDC39': 'lime',
            '#FFEB3B': 'yellow',
            '#FFC107': 'amber',
            '#FF9800': 'orange',
            '#FF5722': 'deep-orange',
            '#795548': 'brown',
            '#9E9E9E': 'grey',
            '#607D8B': 'blue-grey'
        };	

        var accentColorsDictionary = {
            '#FF5252': 'red',
            '#FF4081': 'pink',
            '#E040FB': 'purple',
            '#7C4DFF': 'deep-purple',
            '#536DFE': 'indigo',
            '#448AFF': 'blue',
            '#03A9F4': 'light-blue',
            '#00BCD4': 'cyan',
            '#009688': 'teal',
            '#4CAF50': 'green',
            '#8BC34A': 'light-green',
            '#CDDC39': 'lime',
            '#FFEB3B': 'yellow',
            '#FFC107': 'amber',
            '#FF9800': 'orange',
            '#FF5722': 'deep-orange',
            '#795548': 'brown',
            '#9E9E9E': 'grey',
            '#607D8B': 'blue-grey'
        };	        

        function _resetStyle(){
        	window.localStorage.removeItem('selectedTheme');
        }


        function _setStyle(primaryColor, accentColor){

        	var selectedTheme = window.localStorage.getItem('selectedTheme');

        	if(!primaryColorsDictionary[primaryColor] || !accentColorsDictionary[accentColor] || selectedTheme === 'custom'){
        		return;
        	}


	        var theme = {
	            primary   : {
	                name: primaryColorsDictionary[primaryColor],
	                hues: {
	                    'default': '700',
	                    'hue-1'  : '500',
	                    'hue-2'  : '600',
	                    'hue-3'  : '400'
	                }
	            },
	            accent    : {
	                name: accentColorsDictionary[accentColor],
	                hues: {
	                    'default': '600',
	                    'hue-1'  : '400',
	                    'hue-2'  : '700',
	                    'hue-3'  : 'A100'
	                }
	            },
	            warn      : {
	                name: 'red'
	            },
	            background: {
	                name: 'grey',
	                hues: {
	                    'default': 'A100',
	                    'hue-1'  : 'A100',
	                    'hue-2'  : '100',
	                    'hue-3'  : '300'
	                }
	            }
	        };

	        window.localStorage.setItem('customTheme', JSON.stringify(theme));
	        window.localStorage.setItem('selectedTheme', 'custom');

	        $window.location.reload();

        }


        return service;
    }



})();