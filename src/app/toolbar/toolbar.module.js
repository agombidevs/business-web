(function ()
{
    'use strict';

    angular
        .module('app.toolbar', [])
        .config(config);

    /** @ngInject */
    function config($translatePartialLoaderProvider, msApiProvider)
    {

        // Translation
        $translatePartialLoaderProvider.addPart('app/toolbar');

        //API
        msApiProvider.register('logout', ['businessUsers/logout']);
        msApiProvider.register('agentDetails', ['businessUsers/me']);
    }

})();
