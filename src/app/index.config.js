(function() {
    'use strict';

    angular
        .module('fuse')
        .config(config);

    /** @ngInject */
    function config(envServiceProvider, $translateProvider, $mdDateLocaleProvider, msApiProvider) {
        // Put your common app configurations here


        var localApiUrl = 'http://localhost:8080/api/';

        var stagingApiUrl = 'https://inhibitor.staging.agombi.co.il/api/';
        var stagingChatUrl = 'wss://realtime.staging.agombi.co.il:5280/websocket';

        var productionApiUrl = 'https://inhibitor.agombi.co.il/api/';
        var productionChatUrl = 'wss://realtime.agombi.co.il:5280/websocket';

        // set the domains and variables for each environment
        envServiceProvider.config({
            domains: {
                staging: ['web.staging.agombi.co.il', 'localhost'],
                production: ['web.agombi.co.il'],
                local: ['127.0.0.1']
            },
            vars: {
                local: {
                    baseApiUrl: localApiUrl,
                    webSocketChatUrl: stagingChatUrl
                },
                staging: {
                    baseApiUrl: stagingApiUrl,
                    webSocketChatUrl: stagingChatUrl
                },
                production: {
                    baseApiUrl: productionApiUrl,
                    webSocketChatUrl: productionChatUrl
                },
                development: {
                    baseApiUrl: stagingApiUrl,
                    webSocketChatUrl: stagingChatUrl
                }
            }
        });

        // run the environment check, so the comprobation is made
        // before controllers and services are built
        envServiceProvider.check();



        if (window.cordova) {
            if(window.cordova.isDebug){
                envServiceProvider.set('staging');
            } else {
                envServiceProvider.set('production');
            }
        }



        //Set the baseApiUrl to ApiProvider
        var baseApiUrl = envServiceProvider.read('baseApiUrl');
        msApiProvider.setBaseUrl(baseApiUrl);


        // angular-translate configuration
        $translateProvider.useLoader('$translatePartialLoader', {
            urlTemplate: '{part}/i18n/{lang}.json'
        });
        $translateProvider.preferredLanguage('en');
        $translateProvider.useSanitizeValueStrategy('sce');


        //Fix datepicker labels
        $mdDateLocaleProvider.months = ['ינואר', 'פברואר', 'מרץ', 'אפריל', 'מאי', 'יוני', 'יולי', 'אוגוסט', 'ספטמבר', 'אוקטובר', 'נובמבר', 'דצמבר'];
        $mdDateLocaleProvider.shortMonths = ['ינוא', 'פבר', 'מרץ', 'אפרי', 'מאי', 'יוני', 'יולי', 'אוג', 'ספט', 'אוק', 'נוב', 'דצ'];
        $mdDateLocaleProvider.days = ['ראשון', 'שני', 'שלישי', 'רביעי', 'חמישי', 'שישי', 'שבת'];
        $mdDateLocaleProvider.shortDays = ['א', 'ב', 'ג', 'ד', 'ה', 'ו', 'ז'];

        // Can change week display to start on Monday.
        $mdDateLocaleProvider.firstDayOfWeek = 0;

        // Global parse date function
        $mdDateLocaleProvider.parseDate = function(dateString) {
            var m = moment(dateString, 'DD-MM-YYYY', true);
            return m.isValid() ? m.toDate() : new Date(NaN);
        };

        // Global formatDate function
        $mdDateLocaleProvider.formatDate = function(date) {
            if (!date) {
                return '';
            }
            var m = moment(date);
            return m.isValid() ? m.format('DD-MM-YYYY') : '';
        };



        // // In addition to date display, date components also need localized messages
        // // for aria-labels for screen-reader users.

        // $mdDateLocaleProvider.weekNumberFormatter = function(weekNumber) {
        //   return 'Semaine ' + weekNumber;
        // };

        // $mdDateLocaleProvider.msgCalendar = 'Calendrier';
        // $mdDateLocaleProvider.msgOpenCalendar = 'Ouvrir le calendrier';

        // // You can also set when your calendar begins and ends.
        // $mdDateLocaleProvider.firstRenderableDate = new Date(1776, 6, 4);
        // $mdDateLocaleProvider.lastRenderableDate = new Date(2012, 11, 21);

    }

})();
