# Agombi business - UI for agents that have an app in Agombi

## Building

1. "gulp bump --patch/--minor/--major"

2. "gulp" or "gulp build" (for skipping minify and regular compiling)

3. The next step after the HTML JS and CSS compiling is the mobile app build, for that: cordova build


## Publishing

# For web

- gulp publish:staging or gulp publish:production


# For mobile (production)

Android:
- fastlane android deploy

iOS:
- fastlane ios release


Staging:

Android:
- fastlane android betac

iOS:
- fastlane iOS betac


